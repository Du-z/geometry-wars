#ifndef SERVER_H
#define SERVER_H

#include "winsock2.h"
#include "mswsock.h"

#include <string>
#include <map>

#include "chatPacket.h"

#define VERBOSE false

struct UnconfirmedMsg
{
	std::string msg;
	unsigned int age;
};

struct ClientData
{
	unsigned char userSlot;
	sockaddr_in addr;
	unsigned int heartbeat;
	std::map<unsigned char, UnconfirmedMsg> unconfirmedMsgList;
};

class Server
{
public:
	Server();
	~Server(){}

	void setup(const u_short port);
	void loop();
	void shutdown();

private:

	void recvHandshake(const ChatPacketHandshake* packet, const sockaddr_in& from); // Reciving handshake from client, notifies other users of new user
	void recvHandshakeAck(const ClientData& clientData); // responds to handshake
	void recvClientMsg(const ChatPacketMsgToServer* packet); // recieve message packet and transmits to other clients using broadcastMsg() and confirms arival with recvMsgConfirm()
	void recvMsgConfirm(const ChatPacketMsgAck* packet); // confirms packet message arrival
	void broadcastMsg(const std::string& message); // sends a message packet to all clients
	void recvHeartbeat(const ChatPacketHeartbeat* packet); // heartbeat from server, responds with sendHeartbeat()
	void sendHeartbeat(const ClientData& clientData); // sends a heartbeat to the client
	void recvDisconect(const ChatPacketDisconnect* packet); // disconnect signal from client, notifies all other clients.

	void removeClient(const unsigned char client){removeClient(clientDataList[client]);} // removes client from clientDataList
	void removeClient(const ClientData client); // removes client from clientDataList

	std::map<unsigned char, ClientData> clientDataList;

	u_short m_port;

	sockaddr_in socket_address;
	SOCKET server_socket;
};

#endif // SERVER_H