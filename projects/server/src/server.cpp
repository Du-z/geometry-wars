#include "server.h"

#include <iostream>
#include <sstream>
#include <windows.h>

Server::Server()
{
}

void Server::setup(const u_short port)
{
	m_port = port;

	std::cout << "Starting Winsock2." << std::endl;
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		std::cout << "WSAStartup failed." << std::endl;
		return;
	}

	std::cout << "Opening socket " << port << "." << std::endl;
	server_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(server_socket == SOCKET_ERROR)
	{
		std::cout << "Error Opening socket: Error " << WSAGetLastError() << std::endl;
		return;
	}

	socket_address.sin_family = AF_INET;
	socket_address.sin_addr.s_addr = htonl(INADDR_ANY);
	socket_address.sin_port = htons(port);

	std::cout << "Binding socket." << std::endl;
	if(bind(server_socket, (SOCKADDR*) &socket_address, sizeof(socket_address)) == SOCKET_ERROR)
	{
		std::cout << "bind() failed: Error " << WSAGetLastError() << std::endl;
	}

	DWORD dwBytesReturned = 0;
	BOOL bNewBehavior = FALSE;
	WSAIoctl(server_socket, SIO_UDP_CONNRESET, &bNewBehavior, sizeof(bNewBehavior), NULL, 0, &dwBytesReturned, NULL, NULL);

}

void Server::loop()
{
	std::cout << "Starting server loop." << std::endl;
	std::cout << "Waiting for users to connect." << std::endl;
	
	while(true)
	{
		
		fd_set checksockets;
		checksockets.fd_count = 1;
		checksockets.fd_array[0]=server_socket;

		struct timeval t;
		t.tv_sec = 0;
		t.tv_usec = 0;

		int waiting = select(NULL, &checksockets, NULL, NULL, &t);
		if(waiting > 0)
		{
		
			char buffer[1024]; // something big enough for any valid packet
			sockaddr_in from;
			int fromlength = sizeof(from);
			int result = recvfrom(server_socket, buffer, sizeof(buffer), 0, (SOCKADDR*)&from, &fromlength);  // read the data into the temp buffer
			if (result == SOCKET_ERROR)
			{
				std::cout << "recvfrom failed with error " << WSAGetLastError() << std::endl;
			}
			else if (result > 0)
			{
				ChatPacket* p = (ChatPacket*)buffer;  // cast the buffer's address as a packet pointer.
				switch(p->m_type)  // choose action based on the packet's type field
				{
				case HANDSHAKE:
					{
						recvHandshake((ChatPacketHandshake*)p, from);
						break;
					}
				case MESSAGE_TO_SERVER:
					{
						recvClientMsg((ChatPacketMsgToServer*)p);
						break;
					}
				case HEARTBEAT:
					{
						recvHeartbeat((ChatPacketHeartbeat*)p);
						break;
					}
				case MESSAGE_ACK:
					{
						recvMsgConfirm((ChatPacketMsgAck*)p);
						break;
					}
				case DISCONECT:
					{
						recvDisconect((ChatPacketDisconnect*)p);
						break;
					}
				}
			}
		}

		// make sure that no clients have timed out
		if(clientDataList.size() > 0)
		{
			for (auto it = clientDataList.cbegin(), end = clientDataList.cend(); it != end; ++it)
			{
				ClientData* client = &clientDataList[it->first];
				client->heartbeat += 1;
			
				if(client->heartbeat > 10 * 1000) // if haven't gotten a heartbeat for more than 10 seconds drop it out
				{
					removeClient(it->first);
					break;
				}
			}
		}

		// check for message ack
		for (auto it1 = clientDataList.cbegin(), end = clientDataList.cend(); it1 != end; ++it1)
		{
			ClientData* client = &clientDataList[it1->first];

			for (auto it2 = client->unconfirmedMsgList.cbegin(), end = client->unconfirmedMsgList.cend(); it2 != end; ++it2)
			{
				UnconfirmedMsg* unconfirmedMsg = &client->unconfirmedMsgList[it2->first];

				unconfirmedMsg->age++;
				if(unconfirmedMsg->age > 3 * 1000) // if haven't gotten a confirmation for more than 3 seconds send it again
				{
					unconfirmedMsg->age = 0;

					// send the new msg
					ChatPacketMsgFromServer packet = ChatPacketMsgFromServer(unconfirmedMsg->msg);

					int result = sendto(server_socket, (const char*)&packet, sizeof(packet), 0, (SOCKADDR*)&it1->second.addr, sizeof(it1->second.addr));

					ClientData* client = &clientDataList[it1->first];	
					if(result == SOCKET_ERROR)
					{
						std::cout << "Error: Message Resend could not be sent to user " << (int)it1->second.userSlot << " " << WSAGetLastError() << std::endl;
						return;
					}

					// make a new entry to be confirmed
					UnconfirmedMsg newUnconfirmedMsg;
					newUnconfirmedMsg.age = 0;
					newUnconfirmedMsg.msg = packet.m_message;
					client->unconfirmedMsgList[packet.m_timestamp] = newUnconfirmedMsg;

					// delete the old entry
					client->unconfirmedMsgList.erase(it2);
					break;
				}
			}
		}

		// make sure we don't burn too many CPU cycles
		Sleep(1);
	}
}

void Server::recvMsgConfirm(const ChatPacketMsgAck* packet)
{
	clientDataList[packet->m_slot].unconfirmedMsgList.erase(packet->m_acknowledgeTimestamp);
}

void Server::recvDisconect(const ChatPacketDisconnect* packet)
{
	removeClient(packet->m_slot);
}

void Server::removeClient(const ClientData client)
{
	std::stringstream sstream;
	sstream << "Player " << (int)client.userSlot << " has left the server.";
	
	// make sure we remove the clients before we tell the other clients
	clientDataList.erase(client.userSlot);

	std::cout << sstream.str() << std::endl;
	broadcastMsg(sstream.str());
}

void Server::recvHandshake(const ChatPacketHandshake* packet, const sockaddr_in& from)
{
	// create a new client
	ClientData clientData;
	clientData.addr = from;
	clientData.heartbeat = 0;
	clientData.userSlot = 0;

	// find the first empty slot (good enough for such a small list)
	for (auto it = clientDataList.cbegin(), end = clientDataList.cend();
		it != end && clientData.userSlot == it->first;
		++it, ++clientData.userSlot)
	{}

	// add this client to the slot
	clientDataList[clientData.userSlot] = clientData;

	// tell the client it has been added
	recvHandshakeAck(clientData);
	std::stringstream sstream;
	sstream << "User " << (int)clientData.userSlot << " has joined the server.";
	std::cout << sstream.str() << std::endl;
	broadcastMsg(sstream.str());
}

void Server::recvHandshakeAck(const ClientData& clientData)
{
	ChatPacketHandshakeAck packet = ChatPacketHandshakeAck(clientData.userSlot);

	int result = sendto(server_socket, (const char*)&packet, sizeof(packet), 0, (SOCKADDR*)&clientData.addr, sizeof(clientData.addr));

	if(result == SOCKET_ERROR)
	{
		std::cout << "Error: Handshake acknowledgment could not be sent " << WSAGetLastError() << std::endl;
		return;
	}
}

void Server::recvClientMsg(const ChatPacketMsgToServer* packet)
{
	std::stringstream sstream;

	sstream << packet->m_message;
	std::cout << sstream.str() << std::endl;
	broadcastMsg(sstream.str());
}

void Server::recvHeartbeat(const ChatPacketHeartbeat* packet)
{
	if(clientDataList.find(packet->m_slot) != clientDataList.end())
	{
		ClientData* client = &clientDataList[packet->m_slot];
		client->heartbeat = 0;
		sendHeartbeat(*client);

		if(VERBOSE)
		{
			std::stringstream sstream;
			sstream << "Received heartbeat from " << (int)client->userSlot << ".";
			std::cout << sstream.str() << std::endl;
		}
	}
}

void Server::sendHeartbeat(const ClientData& clientData)
{
	ChatPacketHeartbeat packet(255);

	int result = sendto(server_socket, (const char*)&packet, sizeof(packet), 0, (SOCKADDR*)&clientData.addr, sizeof(clientData.addr));

	if(result == SOCKET_ERROR)
	{
		std::cout << "Error: Heartbeat could not be sent " << WSAGetLastError() << std::endl;
		return;
	}
}

void Server::broadcastMsg(const std::string& message)
{
	ChatPacketMsgFromServer packet = ChatPacketMsgFromServer(message);

	for (auto it = clientDataList.cbegin(), end = clientDataList.cend(); it != end; ++it)
	{
		int result = sendto(server_socket, (const char*)&packet, sizeof(packet), 0, (SOCKADDR*)&it->second.addr, sizeof(it->second.addr));

		UnconfirmedMsg unconfirmedMsg;
		unconfirmedMsg.age = 0;
		unconfirmedMsg.msg = message;
		ClientData* client = &clientDataList[it->first];
		client->unconfirmedMsgList[packet.m_timestamp] = unconfirmedMsg;
	
		if(result == SOCKET_ERROR)
		{
			std::cout << "Error: Broadcast message could not be sent to '" << (int)it->second.userSlot << "' " << WSAGetLastError() << std::endl;
			return;
		}
	
	}
}

void Server::shutdown()
{
	WSACleanup();
}