#ifndef CHAT_PACKET_H
#define CHAT_PACKET_H

#define NAME_SIZE 16
#define MSG_SIZE 100

enum ChatPacketType : unsigned char
{
	HANDSHAKE,
	HANDSHAKE_ACK,
	MESSAGE_TO_SERVER,
	MESSAGE_FROM_SERVER,
	MESSAGE_ACK,
	HEARTBEAT,
	DISCONECT
};

#pragma pack(push,1)

static unsigned char packetNumber = 0;

// MAIN PACKET
class ChatPacket
{
public:
	ChatPacket()
	{
		m_timestamp = packetNumber++;
	}

	ChatPacketType m_type;
	unsigned char m_timestamp;

private:
};

// HANDSHAKE PACKET
class ChatPacketHandshake: public ChatPacket
{
public:
	ChatPacketHandshake()
	{
		m_type = HANDSHAKE;
	}

private:
};

// HANDSHAKE ACKNOWLEDGEMENT PACKET
class ChatPacketHandshakeAck: public ChatPacket
{
public:
	ChatPacketHandshakeAck(const unsigned char slot)
	{
		m_type = HANDSHAKE_ACK;

		m_slot = slot;
	}
	unsigned char m_slot;

private:
	ChatPacketHandshakeAck();
};

// MESSAGE FROM SERVER PACKET
class ChatPacketMsgFromServer: public ChatPacket
{
public:
	ChatPacketMsgFromServer(const std::string& message)
	{
		m_type = MESSAGE_FROM_SERVER;
		strncpy(m_message, message.c_str(), MSG_SIZE - 1);
		m_message[MSG_SIZE - 1] = '\0';
	}
	char m_message[MSG_SIZE];

private:
	ChatPacketMsgFromServer();
};

// MESSAGE TO SERVER PACKET
class ChatPacketMsgToServer: public ChatPacket
{
public:
	ChatPacketMsgToServer(const unsigned char slot, const std::string& message)
	{
		m_type = MESSAGE_TO_SERVER;
		strncpy(m_message, message.c_str(), MSG_SIZE - 1);
		m_slot = slot;
	}
	char m_message[MSG_SIZE];
	unsigned char m_slot;

private:
	ChatPacketMsgToServer();
};

// MESSAGE ACKNOWLEDGEMENT PACKET
class ChatPacketMsgAck: public ChatPacket
{
public:
	ChatPacketMsgAck(const unsigned char slot, const unsigned char acknowledgeTimestamp)
	{
		m_type = MESSAGE_ACK;
		m_acknowledgeTimestamp = acknowledgeTimestamp;
		m_slot = slot;
	}
	unsigned char m_slot;
	unsigned char m_acknowledgeTimestamp;

private:
};

// HEARTBEAT PACKET
class ChatPacketHeartbeat: public ChatPacket
{
public:
	ChatPacketHeartbeat(const unsigned char slot)
	{
		m_type = HEARTBEAT;
		m_slot = slot;
	}
	unsigned char m_slot;

private:
	ChatPacketHeartbeat();
};

// DISCONECT PACKET
class ChatPacketDisconnect: public ChatPacket
{
public:
	ChatPacketDisconnect(const unsigned char slot)
	{
		m_type = DISCONECT;
		m_slot = slot;
	}
	unsigned char m_slot;

private:
	ChatPacketDisconnect();
};

#pragma pack(pop)
#endif // CHAT_PACKET_H