#ifndef H_SCREEN
#define H_SCREEN

#include "SFML\Graphics\RenderWindow.hpp"

#include "app\actorMgr.h"

enum ScreenTypes{NO_CHANGE = 0, MAIN_MENU, GAME_LOOP_SP, GAME_LOOP_MP, MP_LOBBY, HOW_PLAY, EXIT};

class ABScreen
{
public:
	ABScreen();
	virtual ~ABScreen();

	virtual void setup(sf::RenderWindow& window) = 0;
	virtual void simulation(sf::RenderWindow& window, float deltaT) = 0;
	virtual void eventReceiver(ABEvent* theEvent) = 0;
	virtual void render(sf::RenderWindow& window) = 0;
	virtual void dispose() = 0;

	ScreenTypes screenChange(){return nextScreen;}

protected:
	size_t nameHash; // the hash for the screens name string
	ScreenTypes nextScreen; // the screen to switch to (normally NO_CHANGE)

	ActorMgr actorMgr;

private:
};

#endif // H_SCREEN