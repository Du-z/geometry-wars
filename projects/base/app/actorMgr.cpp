#include "app\actorMgr.h"

#include <sstream>
#include "kf\kf_log.h"

ActorMgr::ActorMgr()
{

}

ActorMgr::~ActorMgr()
{
	
}

void ActorMgr::addActors(const Json::Value& screenConfig, b2World* pB2World)
{
	size_t numActors = screenConfig.size();

	std::hash<std::string> hashFn;
	size_t actorNameHash;

	for(size_t i = 0, size = numActors; i < size; ++i)
	{
		actorNameHash = hashFn(screenConfig[i]["name"].asString());

		size_t count = screenConfig[i]["count"].asInt();
		// to be faster it needs a for loop after finding the correct actor, not before
		// but this is during loading, so it is not performance critical
		for(size_t j = 0; j < count; ++j)
		{
			ABActor* actor = NULL;
			
			if (actorNameHash == hashFn("keyboard"))
				actor = new Keyboard();
			else if (actorNameHash == hashFn("mouse"))
				actor = new Mouse();
			else if (actorNameHash == hashFn("background"))
				actor = new Background();
			else if (actorNameHash == hashFn("particleMgr"))
				actor = new ParticleMgr();
			else if (actorNameHash == hashFn("wall"))
				actor = new Wall();
			else if (actorNameHash == hashFn("player"))
				actor = new Player();
			else if (actorNameHash == hashFn("enemyMgr"))
				actor = new EnemyMgr();
			else if (actorNameHash == hashFn("bulletMgr"))
				actor = new BulletMgr();
			else if (actorNameHash == hashFn("uiMgr"))
				actor = new UiMgr();
			else if (actorNameHash == hashFn("hud"))
				actor = new Hud();

			
			if(actor != NULL)
			{
				actor->setup(pB2World, screenConfig[i]["properties"]);
				actorList.push_back(actor);
			}
			else
			{
				kf_log("Could not load actor '" + screenConfig[i]["name"].asString() + "' - Not Defined");
			}

		}
	}
}

void ActorMgr::simulation(sf::RenderWindow& window, float deltaT)
{
	// simulate the vector of actors
	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->simulation(window, deltaT);
	}
}

void ActorMgr::render(sf::RenderWindow& window)
{
	// draw the vector of actors
	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->render(window);
	}
}

void ActorMgr::dispose()
{
	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->dispose();
		delete actorList[i];
	}

	actorList.clear();
}

void ActorMgr::sortActors()
{
	// uses http://en.wikipedia.org/wiki/Selection_sort but we sort the largest, not the smallest.

	size_t size = actorList.size(), loopCount = 0, swaps = 0;

	while(size > 1) // keep sorting until we run out of elements to sort.
	{		
		size_t largest = 0;
		for(size_t i = 0; i < size; ++i) // find the largest.
		{
			if(actorList[i]->getActorType() > actorList[largest]->getActorType())
				largest = i;
			loopCount++;
		}

		size--; // reduce the size to search by 1 because we dont need to check the value at the back because it is the largest (also it can conveniently be used below)

		if(largest != size) // we only have to swap if it is not the last val already
		{
			std::swap(actorList[largest], actorList[size]);
			swaps++;
		}
	}

	std::stringstream ss;
	ss << "Finished sorting Actors...\n\t Number of Actors = " << actorList.size() << "\n\t Number of Checks = " << loopCount << "\n\t Number of Swaps = " << swaps;
	kf_log(ss.str());
}