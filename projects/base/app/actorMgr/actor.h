#ifndef H_ABACTOR
#define H_ABACTOR

#include "SFML\Graphics\RenderWindow.hpp"
#include "Box2D\Box2D.h"
#include "json\value.h"

#include "app\drawable.h"
#include "app\actorMgr\actorEnum.h"
#include "app\eventMgr\event.h"

class ABActor
{
public:
	ABActor();
	virtual ~ABActor();

	Actor::Type getActorType(){return mActorType;}

	virtual void setup(b2World* pB2World, const Json::Value& properties) = 0;
	virtual void simulation(sf::RenderWindow& window, float deltaT) = 0;
	virtual void eventReceiver(ABEvent* theEvent) = 0;
	virtual void render(sf::RenderWindow& window) = 0;
	virtual void dispose() = 0;

protected:
	Actor::Type mActorType; // the enum of the actor

private:
};

#endif // H_ABACTOR