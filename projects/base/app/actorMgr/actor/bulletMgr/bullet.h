#ifndef H_BULLET
#define H_BULLET

#include "app\actorMgr\actor.h"

#include "app\drawable.h"
#include "Box2D\Box2D.h"

class Bullet : public ABActor
{
public:
	Bullet();
	virtual ~Bullet();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void shoot(const b2Vec2& pos, const float radian);

	bool getIsActive(){return isActive;}

private:
	Drawable* pDrawable;
	b2Body* pBody;

	bool isActive;


};

#endif // H_BULLET