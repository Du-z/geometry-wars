#include "app\actorMgr\actor\bulletMgr\bullet.h"

#include "general.h"

#include "app\eventMgr.h"
#include "app\eventMgr\event\collision.h"
#include "app\eventMgr\event\wallHit.h"

Bullet::Bullet()
{
	mActorType = Actor::BULLET;
}

Bullet::~Bullet()
{
}

void Bullet::setup(b2World* pB2World, const Json::Value& properties)
{
	isActive = false;

	/////////////// SETUP Box 2D /////////////////
	b2Vec2 size(2, 2); // Set size here

	b2BodyDef bodyDef;
	bodyDef.position.Set(-64, -64);
	bodyDef.type = b2_dynamicBody;
	bodyDef.fixedRotation = true;
	pBody = pB2World->CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0.7f; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.density = 0.1f;
	fixtureDef.isSensor = true;
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);
	pBody->SetUserData(this);
	//////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/sprites.png");
	pDrawable->setTextureRect(32, 0, 32, 32);
	pDrawable->setSize(size);
	pDrawable->setOrigin(sf::Vector2f((float) pDrawable->getSprite()->getTextureRect().width / 2, (float) pDrawable->getSprite()->getTextureRect().height / 2));
	pDrawable->setColour(sf::Color::Yellow);
	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::B2D_COLLISION, this);
	//////////////////////////////////////////////
}

void Bullet::shoot(const b2Vec2& pos, const float radian)
{
	isActive = true;

	pBody->SetActive(true);
	pBody->SetTransform(pos, radian);

	pBody->SetLinearVelocity(speedRotToB2Vect(80, radian));
}

void Bullet::simulation(sf::RenderWindow& window, float deltaT)
{
	if(isActive)
		pDrawable->setPosRot(*pBody);
	else
		pBody->SetActive(false);
}

void Bullet::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::B2D_COLLISION)
	{
		Collision* collision = static_cast<Collision*>(theEvent);

		if(collision->getOtherBody()->getActorType() != Actor::PLAYER)
		{
			isActive = false;
			if(collision->getOtherBody()->getActorType() == Actor::WALL)
			{
				EventMgr::GetInstance()->addEvent(new WallHit(pBody->GetPosition()));
			}
		}
	}
}

void Bullet::render(sf::RenderWindow& window)
{
	if(isActive)
		pDrawable->draw(window);
}

void Bullet::dispose()
{
	delete pDrawable;
	pDrawable = NULL;

	pBody->GetWorld()->DestroyBody(pBody);
	pBody = NULL;
}
