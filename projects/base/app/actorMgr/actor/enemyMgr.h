#ifndef H_ENEMY_MGR_ACTOR
#define H_ENEMY_MGR_ACTOR

#include "app\actorMgr\actor.h"

#include "app\actorMgr\actor\enemyMgr\enemyPolicy.h"

class EnemyMgr : public ABActor
{
public:
	EnemyMgr();
	virtual ~EnemyMgr();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	std::vector<Drawable*> drawableList;
	std::vector<EnemyPolicy*> enemyList;

	b2Vec2 chooseRandomSpawnPos();
	Enemy::Type chooseEnemyToLaunch();

	float waveCountdown; // how long until the next wave
	float waveCountdownMin, waveCountdownMax; // how long between each wave
	size_t waveCount; // the number of waves so far

	float spamTimer;
	Enemy::Type spamEnemyToLaunch;
};

#endif // H_ENEMY_MGR_ACTOR