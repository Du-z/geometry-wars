#include "app\actorMgr\actor\player.h"

#include "app\soundMgr.h"

#include "app\eventMgr.h"
#include "app\eventMgr\event\keyPressed.h"
#include "app\eventMgr\event\mousePos.h"
#include "app\eventMgr\event\playerShoot.h"
#include "app\eventMgr\event\playerKilled.h"
#include "app\eventMgr\event\playerMove.h"
#include "app\eventMgr\event\collision.h"
#include "app\eventMgr\event\dropBomb.h"

Player::Player()
{
	mActorType = Actor::PLAYER;
}

Player::~Player()
{

}

void Player::setup(b2World* pB2World, const Json::Value& properties)
{

	/////////////// SETUP GENERAL ////////////////
	xPower.Set(2.5f, 0);
	yPower.Set(0, 2.5f);

	maxSpeed = 40;
	angularSpeed = 0.25f;
	worldFriction = 1;

	roundsPerMinute = 400;
	shootCountDown = 60.f / roundsPerMinute;

	currentLives = 3;
	deathCountdown = 0;
	isDead = false;

	playerBombs = 3;
	bombCooldown = 0;

	fastFire = false;
	fastFireChargeMax = 2;
	fastFireCharge = fastFireChargeMax;
	//////////////////////////////////////////////

	/////////////// SETUP Box 2D /////////////////
	b2Vec2 size(4, 4); // Set size here

	b2BodyDef bodyDef;
	bodyDef.position.Set((mapWidth / 2) / worldScale, (mapHeight / 2) / worldScale);
	bodyDef.type = b2_dynamicBody;
	bodyDef.fixedRotation = true;
	pBody = pB2World->CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0.7f; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.density = 0.1f;
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);
	pBody->SetUserData(this);
	//////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/ships.png");
	pDrawable->setTextureRect(0, 0, 32, 32);
	pDrawable->setSize(size);
	pDrawable->setOrigin(sf::Vector2f((float) pDrawable->getSprite()->getTextureRect().width / 2, (float) pDrawable->getSprite()->getTextureRect().height / 2));
	pDrawable->setColour(sf::Color(255, 255, 255, 230));
	pDrawable->setBlendMode(sf::BlendAdd);
	pDrawable->setSmoothing(true);
	//////////////////////////////////////////////

	/////////////// SETUP Sound /////////////////
	SoundMgr::GetInstance()->play("data/sounds/player/spawn.ogg");
	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::KEY_PRESSED, this);
	EventMgr::GetInstance()->addSubscriber(Event::MOUSE_POSITION, this);
	EventMgr::GetInstance()->addSubscriber(Event::B2D_COLLISION, this);
	//////////////////////////////////////////////
}

void Player::simulation(sf::RenderWindow& window, float deltaT)
{
	pDrawable->setPosRot(*pBody);

	sf::View view = window.getView();

	float x = (float)(pBody->GetPosition().x * worldScale) - screenWidth;
	float y = (float)(pBody->GetPosition().y * worldScale) - screenHeight;

	view.reset(sf::FloatRect(x, y, screenWidth * 2, screenHeight * 2));
	window.setView(view);

	////////////// Check for player death //////////////
	
	if(deathCountdown > 0)
	{
		if(!isDead)
			isDead = true;
		deathCountdown -= deltaT;

		pBody->SetLinearVelocity(b2Vec2(0,0));
		pBody->SetAngularVelocity(0);

		EventMgr::GetInstance()->addEvent(
				new PlayerMove(pBody->GetPosition(), pBody->GetAngle(), false)
				);
	}
	else if(isDead)
	{
		pBody->SetTransform(b2Vec2((mapWidth / 2) / worldScale, (mapHeight / 2) / worldScale), 0);
		SoundMgr::GetInstance()->play("data/sounds/player/spawn.ogg");
		
		isDead = false;
	}
	else // if the player isnt dead do all of this other fancy stuff
	{
	////////////////////////////////////////////////////

		////////////// Slow Player Down //////////////

		b2Vec2 vec = pBody->GetLinearVelocity();
		if(vec.x - worldFriction > 0)
			vec.x -= worldFriction;
		else if(vec.x + worldFriction < 0)
			vec.x += worldFriction;
		else
			vec.x = 0;

		if(vec.y - worldFriction > 0)
			vec.y -= worldFriction;
		else if(vec.y + worldFriction < 0)
			vec.y += worldFriction;
		else
			vec.y = 0;
	
		pBody->SetLinearVelocity(vec);
		///////////////////////////////////////////////

		////////////// Check for Overspeed //////////////
		// (from moving at an angle)
		float speed = vectToSpeed(pBody->GetLinearVelocity());

		if(speed > maxSpeed)
		{
			b2Vec2 vec = speedRotToB2Vect(maxSpeed, vectToRadian(pBody->GetLinearVelocity()));
			pBody->SetLinearVelocity(vec);
		}

		// make a move event
		if(speed > 0)
		{
			EventMgr::GetInstance()->addEvent(
				new PlayerMove(pBody->GetPosition(), pBody->GetAngle(), true)
				);
		}
		else
		{
			EventMgr::GetInstance()->addEvent(
				new PlayerMove(pBody->GetPosition(), pBody->GetAngle(), false)
				);
		}
		///////////////////////////////////////////////

		////////////// Set Player Angle //////////////
		if(pBody->GetLinearVelocity().x != 0 || pBody->GetLinearVelocity().y != 0)
		{
			float radNeeded = vectToRadian(pBody->GetLinearVelocity());
			float radCurrent = pBody->GetAngle();

			if(abs(radCurrent - radNeeded) > 3.14159265)
				radNeeded += 6.28318531;

			if(radCurrent + angularSpeed < radNeeded)
				radCurrent += angularSpeed;
			else if(radCurrent - angularSpeed > radNeeded)
				radCurrent -= angularSpeed;
			else
				radCurrent = radNeeded;

			if(radCurrent < 0)
			{
				radCurrent = 6.28318531 + radCurrent;
			}

			pBody->SetTransform(pBody->GetPosition(), fmod(radCurrent, 6.28318531f));
		}
		///////////////////////////////////////////////

		////////////// Shoot if needed //////////////

		if(shootCountDown < 0)
		{
			if(fastFire)
				aimRad += randFloat(-0.15f, 0.15f);

			EventMgr::GetInstance()->addEvent(new PlayerShoot(pBody->GetPosition(), aimRad));
	
			shootCountDown = 60.f / roundsPerMinute;
		}

		if(fastFire)
		{
			shootCountDown -= deltaT * 3;
			fastFireCharge -= deltaT;

			fastFire = false;
		}
		else
		{
			fastFireCharge += deltaT / 2;

			shootCountDown -= deltaT;
		}

		if(fastFireCharge > fastFireChargeMax)
			fastFireCharge = fastFireChargeMax;
		else if(fastFireCharge < 0)
			fastFireCharge = 0;

		float colour = 255 * (fastFireCharge / fastFireChargeMax);
		pDrawable->setColour(sf::Color(255, colour, colour, 230));

		///////////////////////////////////////////////

		bombCooldown -= deltaT;
	}
}

void Player::eventReceiver(ABEvent* theEvent)
{
	// Check the event type
	if(theEvent->getEventType() == Event::KEY_PRESSED)
	{
		KeyPressed* key = static_cast<KeyPressed*>(theEvent);

		if(!isDead)
		{
			switch (key->getKey())
			{
			case sf::Keyboard::D:
				if(pBody->GetLinearVelocity().x < maxSpeed)
					pBody->ApplyLinearImpulse(xPower, pBody->GetWorldCenter());
				break;
			case sf::Keyboard::A:
				if(pBody->GetLinearVelocity().x > -maxSpeed)
					pBody->ApplyLinearImpulse(-xPower, pBody->GetWorldCenter());
				break;
			case sf::Keyboard::W:
				if(pBody->GetLinearVelocity().y > -maxSpeed)
					pBody->ApplyLinearImpulse(-yPower, pBody->GetWorldCenter());
				break;
			case sf::Keyboard::S:
				if(pBody->GetLinearVelocity().y < maxSpeed)
					pBody->ApplyLinearImpulse(yPower, pBody->GetWorldCenter());
				break;
			case sf::Keyboard::LShift:
				if(fastFireCharge > 0.01f)
					fastFire = true;
				break;
			case sf::Keyboard::Space:
				if(bombCooldown < 0 && playerBombs > 0)
				{
					bombCooldown = 3;
					playerBombs--;
					EventMgr::GetInstance()->addEvent(new DropBomb(pBody->GetPosition(), playerBombs));
					SoundMgr::GetInstance()->play("data/sounds/player/bomb.ogg");
				}
				break;
			}
		}
	}
	else if(theEvent->getEventType() == Event::MOUSE_POSITION)
	{
		MousePos* mousePos = static_cast<MousePos*>(theEvent);
	
		aimRad = vectToRadian(
			b2Vec2(mousePos->getPos().x / worldScale, mousePos->getPos().y / worldScale), 
			pBody->GetPosition());
	}
	else if(theEvent->getEventType() == Event::B2D_COLLISION)
	{
		Collision* collision = static_cast<Collision*>(theEvent);

		if(!isDead && collision->getOtherBody()->getActorType() == Actor::ENEMY)
		{
			currentLives--;
			deathCountdown = 2;
			EventMgr::GetInstance()->addEvent(new PlayerKilled(pBody->GetPosition(), currentLives));
			SoundMgr::GetInstance()->play("data/sounds/player/explode.ogg");
			fastFireCharge = fastFireChargeMax;
		}
	}
}

void Player::render(sf::RenderWindow& window)
{
	if(!isDead)
		pDrawable->draw(window);
}

void Player::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;

	pBody->GetWorld()->DestroyBody(pBody);
	pBody = NULL;
}
