#ifndef H_BULLET_MGR_ACTOR
#define H_BULLET_MGR_ACTOR

#include "app\actorMgr\actor.h"

#include "app\actorMgr\actor\bulletMgr\bullet.h"

class BulletMgr : public ABActor
{
public:
	BulletMgr();
	virtual ~BulletMgr();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:

	std::vector<Bullet> bulletList;

};

#endif // H_BULLET_MGR_ACTOR