#include "app\actorMgr\actor\background.h"

Background::Background()
{
	mActorType = Actor::BACKGROUND;
}

Background::~Background()
{

}

void Background::setup(b2World* pB2World, const Json::Value& properties)
{
	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/bg.png");
	pDrawable->setPos(sf::Vector2f(0, 0));
	pDrawable->setSize(sf::Vector2f(mapHeight, mapWidth));

	pDrawable->setColour(sf::Color(50, 50, 50, 255));
	//////////////////////////////////////////////
}

void Background::simulation(sf::RenderWindow& window, float deltaT)
{

}

void Background::eventReceiver(ABEvent* theEvent)
{

}

void Background::render(sf::RenderWindow& window)
{
	pDrawable->draw(window);
}

void Background::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;
}
