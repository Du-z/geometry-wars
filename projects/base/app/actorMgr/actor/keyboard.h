#ifndef H_KEYBOARD_ACTOR
#define H_KEYBOARD_ACTOR

#include "app\actorMgr\actor.h"

class Keyboard : public ABActor
{
public:
	Keyboard();
	virtual ~Keyboard();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
};

#endif // H_KEYBOARD_ACTOR