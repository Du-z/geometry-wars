#include "app\actorMgr\actor\hud.h"

#include "app\eventMgr.h"
#include "app\eventMgr\event\playerKilled.h"
#include "app\eventMgr\event\enemyKilled.h"
#include "app\eventMgr\event\dropBomb.h"

#include <sstream>

Hud::Hud()
{
	mActorType = Actor::HUD;
}

Hud::~Hud()
{

}

void Hud::setup(b2World* pB2World, const Json::Value& properties)
{
	scoreLbl.setup("scoreLbl", "Score: N/A", "data/arial.ttf",
		sf::Vector2f(0, 0), 70, sf::Color(255, 102, 0, 230));

	livesLbl.setup("livesLbl", "Lives: N/A", "data/arial.ttf",
		sf::Vector2f(0, 0), 70, sf::Color(255, 102, 0, 230));

	bombsLbl.setup("bombsLbl", "Bombs: N/A", "data/arial.ttf",
		sf::Vector2f(0, 0), 70, sf::Color(255, 102, 0, 230));

	pointLbl.setup("pointLbl", "N/A", "data/arial.ttf",
		sf::Vector2f(0, 0), 40, sf::Color(0, 255, 0, 230));

	score = 0;
	lives = 3;
	bombs = 3;

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::PLAYER_KILLED, this);
	EventMgr::GetInstance()->addSubscriber(Event::ENEMY_KILLED, this);
	EventMgr::GetInstance()->addSubscriber(Event::DROP_BOMB, this);
	//////////////////////////////////////////////
}

void Hud::simulation(sf::RenderWindow& window, float deltaT)
{
	
	sf::Vector2f offset = window.getView().getCenter();
	offset.x = offset.x - screenWidth;
	offset.y = offset.y - screenHeight;
	
	scoreLbl.setPos(sf::Vector2f(screenWidth / 2 + offset.x, 50 + offset.y));
	livesLbl.setPos(sf::Vector2f(screenWidth + offset.x, 50 + offset.y));
	bombsLbl.setPos(sf::Vector2f(screenWidth + screenWidth / 2 + offset.x, 50 + offset.y));


	std::stringstream ss;

	ss << "Score: " << score;
	scoreLbl.setString(ss.str());
	ss.str(std::string());

	ss << "Lives: " << lives;
	livesLbl.setString(ss.str());
	ss.str(std::string());

	ss << "Bombs: " << bombs;
	bombsLbl.setString(ss.str());
	ss.str(std::string());

	for(int i = 0; i < pointLblList.size(); i++)
	{
		pointLblList[i].deathTime -= deltaT;
	}
	
	// the life will be up for the element closest to the front, if at all.
	if(pointLblList.size() > 0 && pointLblList[0].deathTime < 0)
		pointLblList.pop_front();
}

void Hud::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::PLAYER_KILLED)
	{
		PlayerKilled* playerKilled = static_cast<PlayerKilled*>(theEvent);

		lives = playerKilled->getLives();
	}
	else if(theEvent->getEventType() == Event::ENEMY_KILLED)
	{
		EnemyKilled* enemyKilled = static_cast<EnemyKilled*>(theEvent);

		if(enemyKilled->getKilledByPlayer())
		{
			int addScore = 0;
			switch (enemyKilled->getEnemyType())
			{
			case Enemy::WANDERER:
				addScore = 20;
				break;
			case Enemy::GRUNT:
				addScore = 40;
				break;
			case Enemy::WEAVER:
				addScore = 60;
				break;
			case Enemy::SNAKE:
				addScore = 80;
				break;
			case Enemy::GRAVITY_WELL:
				addScore = 100;
				break;
			}
			score += addScore;

			pointLblPod pod;
			std::stringstream ss;
			ss << addScore;

			pod.points = ss.str();
			pod.deathTime = 1.5f;
			pod.pos.x = enemyKilled->getPos().x * worldScale;
			pod.pos.y = enemyKilled->getPos().y * worldScale;
			pointLblList.push_back(pod);
		}
	}
	else if(theEvent->getEventType() == Event::DROP_BOMB)
	{
		DropBomb* dropBomb = static_cast<DropBomb*>(theEvent);

		bombs = dropBomb->getBombs();
	}
}

void Hud::render(sf::RenderWindow& window)
{
	scoreLbl.render(window);
	livesLbl.render(window);
	bombsLbl.render(window);

	for(int i = 0; i < pointLblList.size(); i++)
	{
		pointLbl.setString(pointLblList[i].points);
		pointLbl.setPos(pointLblList[i].pos);
		pointLbl.render(window);
	}
}

void Hud::dispose()
{
	scoreLbl.dispose();
	livesLbl.dispose();
	bombsLbl.dispose();
}
