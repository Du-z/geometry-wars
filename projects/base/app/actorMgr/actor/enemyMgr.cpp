#include "app\actorMgr\actor\enemyMgr.h"

#include "kf\kf_log.h"

#include "app\soundMgr.h"

#include "app\eventMgr.h"
#include "app\eventMgr\event\playerKilled.h"
#include "app\screens\gameLoop.h"
#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\snake.h"

#include "app\resourceMgr.h"

EnemyMgr::EnemyMgr()
{
	mActorType = Actor::ENEMY_MGR;
}

EnemyMgr::~EnemyMgr()
{

}

void EnemyMgr::setup(b2World* pB2World, const Json::Value& properties)
{	
	waveCount = 0;
	waveCountdownMin = 0.5;
	waveCountdownMax = 1.5;
	waveCountdown = 2;

	spamTimer = -1;
	spamEnemyToLaunch = Enemy::UNSET;
	
	enemyList.resize(1);

	for(size_t i = 0; i < enemyList.size(); ++i)
	{
		enemyList[i] = new EnemyPolicy;
		enemyList[i]->setup(pB2World, properties);
	}

	/////////////// SETUP DRAWABLES ///////////////
	b2Vec2 size(4, 4);
	drawableList.resize(Enemy::COUNT);

	drawableList[Enemy::WANDERER] = new Drawable;
	drawableList[Enemy::WANDERER]->setup("data/ships.png");
	drawableList[Enemy::WANDERER]->setTextureRect(64, 0, 32, 32);
	drawableList[Enemy::WANDERER]->setSize(size);
	drawableList[Enemy::WANDERER]->setOrigin(sf::Vector2f((float) drawableList[Enemy::WANDERER]->getSprite()->getTextureRect().width / 2,
		(float) drawableList[Enemy::WANDERER]->getSprite()->getTextureRect().height / 2));
	drawableList[Enemy::WANDERER]->setColour(sf::Color(216, 110, 235, 230));
	drawableList[Enemy::WANDERER]->setBlendMode(sf::BlendAdd);

	drawableList[Enemy::GRUNT] = new Drawable;
	drawableList[Enemy::GRUNT]->setup("data/ships.png");
	drawableList[Enemy::GRUNT]->setTextureRect(32, 0, 32, 32);
	drawableList[Enemy::GRUNT]->setSize(size);
	drawableList[Enemy::GRUNT]->setOrigin(sf::Vector2f((float) drawableList[Enemy::GRUNT]->getSprite()->getTextureRect().width / 2,
		(float) drawableList[Enemy::GRUNT]->getSprite()->getTextureRect().height / 2));
	drawableList[Enemy::GRUNT]->setColour(sf::Color(88, 182, 232, 230));
	drawableList[Enemy::GRUNT]->setBlendMode(sf::BlendAdd);

	drawableList[Enemy::WEAVER] = new Drawable;
	drawableList[Enemy::WEAVER]->setup("data/ships.png");
	drawableList[Enemy::WEAVER]->setTextureRect(96, 0, 32, 32);
	drawableList[Enemy::WEAVER]->setSize(size);
	drawableList[Enemy::WEAVER]->setOrigin(sf::Vector2f((float) drawableList[Enemy::WEAVER]->getSprite()->getTextureRect().width / 2,
		(float) drawableList[Enemy::WEAVER]->getSprite()->getTextureRect().height / 2));
	drawableList[Enemy::WEAVER]->setColour(sf::Color(148, 238, 132, 230));
	drawableList[Enemy::WEAVER]->setBlendMode(sf::BlendAdd);

	drawableList[Enemy::SNAKE] = new Drawable;
	drawableList[Enemy::SNAKE]->setup("data/ships.png");
	drawableList[Enemy::SNAKE]->setTextureRect(0, 64, 32, 32);
	drawableList[Enemy::SNAKE]->setSize(size);
	drawableList[Enemy::SNAKE]->setOrigin(sf::Vector2f((float) drawableList[Enemy::SNAKE]->getSprite()->getTextureRect().width / 2,
		(float) drawableList[Enemy::SNAKE]->getSprite()->getTextureRect().height / 2));
	drawableList[Enemy::SNAKE]->setColour(sf::Color(102, 0, 153, 230));
	drawableList[Enemy::SNAKE]->setBlendMode(sf::BlendAdd);

	size.Set(3, 3);
	Snake::segDrawable = new Drawable;
	Snake::segDrawable->setup("data/ships.png");
	Snake::segDrawable->setTextureRect(32, 64, 32, 32);
	Snake::segDrawable->setSize(size);
	Snake::segDrawable->setOrigin(sf::Vector2f((float) Snake::segDrawable->getSprite()->getTextureRect().width / 2,
		(float) Snake::segDrawable->getSprite()->getTextureRect().height / 2));
	Snake::segDrawable->setColour(sf::Color(255, 200, 0, 230));
	Snake::segDrawable->setBlendMode(sf::BlendAdd);

	size.Set(6, 6);
	drawableList[Enemy::GRAVITY_WELL] = new Drawable;
	drawableList[Enemy::GRAVITY_WELL]->setup("data/ships.png");
	drawableList[Enemy::GRAVITY_WELL]->setTextureRect(64, 32, 32, 32);
	drawableList[Enemy::GRAVITY_WELL]->setSize(size);
	drawableList[Enemy::GRAVITY_WELL]->setOrigin(sf::Vector2f((float) drawableList[Enemy::GRAVITY_WELL]->getSprite()->getTextureRect().width / 2,
		(float) drawableList[Enemy::GRAVITY_WELL]->getSprite()->getTextureRect().height / 2));
	drawableList[Enemy::GRAVITY_WELL]->setColour(sf::Color(255, 0, 0, 230));
	drawableList[Enemy::GRAVITY_WELL]->setBlendMode(sf::BlendAdd);
	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::PLAYER_KILLED, this);
	EventMgr::GetInstance()->addSubscriber(Event::DROP_BOMB, this);
	//////////////////////////////////////////////
}

void EnemyMgr::simulation(sf::RenderWindow& window, float deltaT)
{
	// see if its time for the next wave
	if(waveCountdown < 0)
	{
		waveCount++;
		size_t numShips = 1;

		Enemy::Type toLaunch = Enemy::UNSET;

		if(spamTimer > 0)
		{
			toLaunch = spamEnemyToLaunch;
		}

		for(size_t i = 0; i < numShips; ++i)
		{
			if(spamTimer < 0)
			{
				toLaunch = chooseEnemyToLaunch();

				if(waveCountdownMin > 0.2f)
					waveCountdownMin -= 0.005f;
				if(waveCountdownMax > 0.4f)
					waveCountdownMax -= 0.005f;
			}
			
			b2Vec2 pos = chooseRandomSpawnPos();
			
			if(i < enemyList.size())
			{
				if(!enemyList[i]->getIsActive())
				{
					enemyList[i]->launch(pos, toLaunch, drawableList);
				}
				else
				{
					++numShips;
				}
			}
			else
			{
				EnemyPolicy* enemy = new EnemyPolicy;
				enemy->setup(GameLoop::pB2World, NULL);
				enemy->launch(pos, toLaunch, drawableList);
				enemyList.push_back(enemy);
			}
		}

		if(spamTimer < 0)
		{
			waveCountdown = randFloat(waveCountdownMin, waveCountdownMax);
		}
		else
		{
			waveCountdown = 0.05f;
		}
		
	}
	else
	{
		waveCountdown -= deltaT;
	}
	
	for(size_t i = 0; i < enemyList.size(); ++i)
	{
		enemyList[i]->simulation(window, deltaT);
	}

	spamTimer -= deltaT;
}

b2Vec2 EnemyMgr::chooseRandomSpawnPos()
{
	b2Vec2 pos;

	int rand = randInt(0, 3);

	if(rand == 0)
		pos.Set(10, 10);
	else if(rand == 1)
		pos.Set(mapHeight / worldScale - 10, 10);
	else if(rand == 2)
		pos.Set(10, mapWidth / worldScale - 10);
	else
		pos.Set(mapHeight / worldScale - 10, mapWidth / worldScale - 10);
	
	return pos;
}

Enemy::Type EnemyMgr::chooseEnemyToLaunch()
{
	
	int launchCode = randInt(0, 100);

	if(launchCode >= 100)
	{
		return Enemy::GRAVITY_WELL;
	}
	else if(launchCode >= 95)
	{
		return Enemy::SNAKE;
	}
	else if(launchCode >= 70)
	{
		if(spamTimer < -40 && randInt(0, 100) == 0)
		{
			spamTimer = 3;
			spamEnemyToLaunch = Enemy::WEAVER;
			SoundMgr::GetInstance()->play("data/sounds/general/warning.ogg");
		}
		return Enemy::WEAVER;
	}
	else if(launchCode >= 40)
	{
		if(spamTimer < -40 && randInt(0, 100) == 0)
		{
			spamTimer = 3;
			spamEnemyToLaunch = Enemy::GRUNT;
			SoundMgr::GetInstance()->play("data/sounds/general/warning.ogg");
		}
		return Enemy::GRUNT;
	}
	else
	{
		if(spamTimer < -40 && randInt(0, 100) == 0)
		{
			spamTimer = 3;
			spamEnemyToLaunch = Enemy::WANDERER;
			SoundMgr::GetInstance()->play("data/sounds/general/warning.ogg");
		}
		return Enemy::WANDERER;
	}
}

void EnemyMgr::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::PLAYER_KILLED)
	{
		// add 3 seconds to the spawn timer.
		waveCountdown += 3;
	}
	else if(theEvent->getEventType() == Event::DROP_BOMB)
	{
		// add 2 seconds to the spawn timer.
		waveCountdown += 2;
	}
}

void EnemyMgr::render(sf::RenderWindow& window)
{
	for(size_t i = 0; i < enemyList.size(); ++i)
	{
		enemyList[i]->render(window);
	}
}

void EnemyMgr::dispose()
{
	for(size_t i = 0; i < enemyList.size(); ++i)
	{
		enemyList[i]->dispose();
		delete enemyList[i];
	}
	enemyList.clear();

	for(size_t i = 0; i < drawableList.size(); ++i)
	{
		drawableList[i]->dispose();
		delete drawableList[i];
	}
	drawableList.clear();
}
