#include "app\actorMgr\actor\bulletMgr.h"

#include "kf\kf_log.h"

#include "app\soundMgr.h"

#include "app\eventMgr.h"
#include "app\eventMgr\event\playerShoot.h"

#include "app\resourceMgr.h"

BulletMgr::BulletMgr()
{
	mActorType = Actor::BULLET_MGR;
}

BulletMgr::~BulletMgr()
{

}

void BulletMgr::setup(b2World* pB2World, const Json::Value& properties)
{
	int bulletCount = properties["bulletCount"].asInt();

	bulletList.resize(bulletCount);

	for(size_t i = 0; i < bulletList.size(); ++i)
	{
		bulletList[i].setup(pB2World, properties);
	}


	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::PLAYER_SHOOT, this);
	//////////////////////////////////////////////
}

void BulletMgr::simulation(sf::RenderWindow& window, float deltaT)
{
	for(size_t i = 0; i < bulletList.size(); ++i)
	{
		bulletList[i].simulation(window, deltaT);
	}
}

void BulletMgr::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::PLAYER_SHOOT)
	{
		PlayerShoot* playerShoot = static_cast<PlayerShoot*>(theEvent);

		// find a unused bullet
		for(size_t i = 0; i < bulletList.size(); ++i)
		{
			if(!bulletList[i].getIsActive())
			{
				bulletList[i].shoot(playerShoot->getPos(), playerShoot->getRadian());
				SoundMgr::GetInstance()->play("data/sounds/player/fireNormal.ogg");

				return;
			}
		}

		kf_log("Bullet Mgr has run out of bullets");
	}
}

void BulletMgr::render(sf::RenderWindow& window)
{
	for(size_t i = 0; i < bulletList.size(); ++i)
	{
		bulletList[i].render(window);
	}
}

void BulletMgr::dispose()
{
	for(size_t i = 0; i < bulletList.size(); ++i)
	{
		bulletList[i].dispose();
	}

	bulletList.clear();
}
