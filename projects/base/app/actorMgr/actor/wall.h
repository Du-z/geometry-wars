#ifndef H_WALL_ACTOR
#define H_WALL_ACTOR

#include "app\actorMgr\actor.h"

class Wall : public ABActor
{
public:
	Wall();
	virtual ~Wall();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	Drawable* pDrawable;
	b2Body* pBody;
};

#endif // H_WALL_ACTOR