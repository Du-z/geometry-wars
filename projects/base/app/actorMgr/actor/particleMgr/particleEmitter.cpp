#define _USE_MATH_DEFINES // to enable non-std math consts
#include "app\actorMgr\actor\particleMgr\particleEmitter.h"

#include <functional>

#include "kf\kf_log.h"
#include "general.h"
ParticleEmitter::ParticleEmitter()
{
	isActive = false;

	nameHash = -1;
}

ParticleEmitter::~ParticleEmitter()
{

}

void ParticleEmitter::setup(const Json::Value& particleConfig)
{
	/////////////// SETUP GENERAL ////////////////
	std::hash<std::string> hashFn;
	nameHash = hashFn(particleConfig["name"].asString());
	
	sprayAngle = (float)particleConfig["sprayAngle"].asDouble() / 180 * (float)M_PI;
	speedMin = (float)particleConfig["speed"]["min"].asDouble() * worldScale;
	speedMax = (float)particleConfig["speed"]["max"].asDouble() * worldScale;
	spawnRate = (float)particleConfig["spawnRate"].asDouble();
	life = (float)particleConfig["life"].asDouble();

	spinMin = (float)particleConfig["spin"]["min"].asDouble();
	spinMax = (float)particleConfig["spin"]["max"].asDouble();
	///////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ////////////////
	mDrawable.setup(particleConfig["textureDir"].asString());

	mDrawable.setTextureRect(
		particleConfig["textureRect"]["left"].asInt(),
		particleConfig["textureRect"]["top"].asInt(),
		particleConfig["textureRect"]["width"].asInt(),
		particleConfig["textureRect"]["height"].asInt()
		);

	mDrawable.setSize(sf::Vector2f(
		(float)particleConfig["size"]["x"].asDouble() * worldScale,
		(float)particleConfig["size"]["y"].asDouble() * worldScale)
		);

	mDrawable.setOrigin(sf::Vector2f(
		(float) mDrawable.getSprite()->getTextureRect().width / 2,
		(float) mDrawable.getSprite()->getTextureRect().height / 2)
		);

	colourMin = (sf::Color(
		particleConfig["colour"]["min"]["r"].asInt(),
		particleConfig["colour"]["min"]["g"].asInt(),
		particleConfig["colour"]["min"]["b"].asInt(),
		particleConfig["colour"]["min"]["a"].asInt()
		));

	colourMax = (sf::Color(
		particleConfig["colour"]["max"]["r"].asInt(),
		particleConfig["colour"]["max"]["g"].asInt(),
		particleConfig["colour"]["max"]["b"].asInt(),
		particleConfig["colour"]["max"]["a"].asInt()
		));

	int blendMode = particleConfig["blendMode"].asInt();
	if(blendMode == 0)
		mDrawable.setBlendMode(sf::BlendAlpha);
	else if(blendMode == 1)
		mDrawable.setBlendMode(sf::BlendAdd);
	else if(blendMode == 2)
		mDrawable.setBlendMode(sf::BlendMultiply);
	else if(blendMode == 3)
		mDrawable.setBlendMode(sf::BlendNone);
	else
	{
		mDrawable.setBlendMode(sf::BlendAlpha);
		kf_log("Invalid blend mode for " + particleConfig["name"].asString());
	}
	///////////////////////////////////////////////

	///////////// SETUP PARTICLE LIST /////////////
	particleList.resize(particleConfig["count"].asInt());

	for(size_t i = 0; i < particleList.size(); ++i)
	{
		particleList[i].setup(mDrawable);
	}
	///////////////////////////////////////////////
}

void ParticleEmitter::start(const sf::Vector2f& pos, float angleDirection)
{
	isActive = true;
	particlesStarted = 0;
	particlesKilled = 0;
	numToSpawn = 0;
	particleReset = -1;
	spawnCountdown = 0;
	deathCountdown = life;

	mSpawnPos = pos;
	mAngleDirection = angleDirection;

	for(size_t i = 0; i < particleList.size(); ++i)
	{
		float randSpd = randFloat(speedMin, speedMax);
		float randDir = randFloat(-sprayAngle / 2 - angleDirection, sprayAngle / 2 - angleDirection);
		float randSpin = randFloat(spinMin, spinMax);
		sf::Color randCol = randColour(colourMin, colourMax);
		
		particleList[i].start(pos, randSpd, randDir, randSpin, randCol);
	}
}

void ParticleEmitter::simulation(float deltaT)
{
	// check to see if more particles should be spawned
	if(isActive)
	{
		// if a particle is needed next frame spawn it now
		if(spawnCountdown <= 0) 
		{
			// add it to the current particlesStarted count, but make sure it doesn't overflow
			numToSpawn = (int)(deltaT / spawnRate + 1);
			
			if(particlesStarted < particleList.size())
			{
				particlesStarted += numToSpawn;

				if(particlesStarted > particleList.size())
					particlesStarted = particleList.size();
			}
			else
			{
				particleReset = particleReset + numToSpawn;
			}

			spawnCountdown = spawnRate;
		}
		else
		{
			spawnCountdown -= deltaT; // countdown the time till the next spawn
		}

		// if a particle has a life lets see if we can start killing them
		if(life > 0)
		{
			if(deathCountdown <= 0)
			{
				// figure out what particles need to die
				particlesKilled = (int)fabs(deathCountdown / spawnRate) + 1;

				// check that all the particles that need to be spawned are now dead
				if(particlesKilled >= particleList.size()) 
				{
					isActive = false;
					return;
				}
			}
			deathCountdown -= deltaT;
		}
		// however if life == 0 it is set to respawn as we run out of particles, if they never die, and get moved back to the origPos
		else
		{
			if(numToSpawn > 0)
			{
				for(size_t i = 0; i < numToSpawn; ++i)
				{
					float randSpd = randFloat(speedMin, speedMax);
					float randDir = randFloat(-sprayAngle / 2 - mAngleDirection, sprayAngle / 2 - mAngleDirection);
					float randSpin = randFloat(spinMin, spinMax);
					sf::Color randCol = randColour(colourMin, colourMax);

					int particleI = (particleReset - numToSpawn + i) % particleList.size();
					particleList[particleI].start(mSpawnPos, randSpd, randDir, randSpin, randCol);
				}
			}
		}
		
		for(size_t i = particlesKilled; i < particlesStarted; ++i)
		{
			particleList[i].simulation(deltaT);
		}
	}
}

void ParticleEmitter::render(sf::RenderWindow& window)
{
	if(isActive)
	{
		for(size_t i = particlesKilled; i < particlesStarted; ++i)
		{
			particleList[i].render(window);
		}
	}
}

void ParticleEmitter::dispose()
{
	particleList.clear();
	mDrawable.dispose();
}
