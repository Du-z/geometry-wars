#include "app\actorMgr\actor\particleMgr\particle.h"

#include "general.h"

Particle::Particle()
{
}

Particle::~Particle()
{
}

void Particle::setup(Drawable& drawable)
{
	pDrawable = &drawable;
}

void Particle::start( const sf::Vector2f& pos, float speed, float radian, float spin, sf::Color colour)
{
	mVel = speedRotToSFVect(speed, radian);
	mPos = pos;
	mSpin = spin;
	mRotation = radToDeg(radian) - 90;
	mColour = colour;
}