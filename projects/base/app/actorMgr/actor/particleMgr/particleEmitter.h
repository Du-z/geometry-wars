#ifndef H_PARTICLE_EMITTER
#define H_PARTICLE_EMITTER

#include "json\value.h"
#include "app\actorMgr\actor\particleMgr\particle.h"

class ParticleEmitter
{
public:
	ParticleEmitter();
	~ParticleEmitter();

	void setup(const Json::Value& particleConfig);
	void start(const b2Vec2& pos, float angleDirection){start(sf::Vector2f(pos.x * worldScale, pos.y * worldScale), angleDirection);};
	void start(const sf::Vector2f& pos, float angleDirection);
	void stop(){isActive = false;}
	void simulation(float deltaT);
	void render(sf::RenderWindow& window);
	void dispose();

	bool getIsActive(){return isActive;};
	size_t getNameHash(){return nameHash;}

	void setPos(b2Vec2 pos){setPos(sf::Vector2f(pos.x * worldScale, pos.y * worldScale));}
	void setPos(sf::Vector2f pos){mSpawnPos = pos;}

	void setAngleDirection(float rad){mAngleDirection = rad;}

private:
	bool isActive;

	size_t nameHash; // the hash for the particle emitters name string

	Drawable mDrawable;

	sf::Vector2f mSpawnPos;
	float mAngleDirection; // in radians

	size_t particlesStarted;
	size_t particlesKilled;
	size_t numToSpawn; // how many particles to spawn per frame
	size_t particleReset; // used when a particle never dies
	float spawnCountdown; // countdowns until next particle spawn
	float deathCountdown; // countdowns until the particles start to die

	// from config file
	float spawnRate;
	float life; // how long the particle should live for
	float sprayAngle; // in radians
	float speedMin, speedMax; // in m/s
	float spinMin, spinMax; // in deg/s
	sf::Color colourMin, colourMax;

	std::vector<Particle> particleList;

};

#endif // H_PARTICLE_EMITTER