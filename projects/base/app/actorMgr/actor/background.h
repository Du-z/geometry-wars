#ifndef H_BACKGROUND_ACTOR
#define H_BACKGROUND_ACTOR

#include "app\actorMgr\actor.h"

class Background : public ABActor
{
public:
	Background();
	virtual ~Background();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	Drawable* pDrawable;
};

#endif // H_BACKGROUND_ACTOR