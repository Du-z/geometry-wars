#ifndef H_UI_ENUM
#define H_UI_ENUM

// the game loop will sort the actor list into the same order that these enums are in.
namespace Ui
{
	enum Type
	{
		UNSET = 0, 
		LABEL,
		NINE_PATCH,
		BUTTON,
		COUNT
	};
}

#endif // H_UI_ENUM