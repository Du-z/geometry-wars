#ifndef H_UI_ELEMENT
#define H_UI_ELEMENT

#include "SFML\Graphics\RenderWindow.hpp"
#include "json\value.h"

#include "app\actorMgr\actor\uiMgr\uiEnum.h"
#include "app\eventMgr\event.h"

class ABUiElement
{
public:
	ABUiElement();
	virtual ~ABUiElement();

	virtual void setup(const Json::Value& properties) = 0;
	virtual void simulation(sf::RenderWindow& window, float deltaT) = 0;
	virtual void eventReceiver(ABEvent* theEvent) = 0;
	virtual void render(sf::RenderWindow& window) = 0;
	virtual void dispose() = 0;

	size_t getNameHash(){return nameHash;}
	Ui::Type getUiType(){return mUiType;}

protected:
	size_t nameHash; // the hash for the UI Element name string
	Ui::Type mUiType; // the enum of the actor

private:
};

#endif // H_UI_ELEMENT