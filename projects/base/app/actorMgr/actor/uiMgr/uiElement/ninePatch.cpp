#include "app\actorMgr\actor\uiMgr\uiElement\ninePatch.h"

#include "app\eventMgr\event\mousePos.h"
#include "app\eventMgr\event\mousePressed.h"

#include "app\resourceMgr.h"
#include "general.h"

NinePatch::NinePatch()
{
	mUiType = Ui::NINE_PATCH;
}

NinePatch::~NinePatch()
{

}

void NinePatch::setup(const Json::Value& properties)
{
	spriteList.resize(9);
	
	std::hash<std::string> hashFn;
	nameHash = hashFn(properties["name"].asString());
	
	left = properties["patches"]["left"].asInt();
	top = properties["patches"]["top"].asInt();
	right = properties["patches"]["right"].asInt();
	bottom = properties["patches"]["bottom"].asInt();
	
	for(size_t i = 0; i < spriteList.size(); ++i)
	{
		spriteList[i] = new Drawable();
		spriteList[i]->setup(properties["texDir"].asString());
	}

	sf::Vector2u texSize = spriteList[1]->getSprite()->getTexture()->getSize();
	
	// Top Left
	spriteList[0]->setTextureRect(0, 0, left, top);
	// Top Center
	spriteList[1]->setTextureRect(left, 0, texSize.x - left - right, top);
	// Top Right
	spriteList[2]->setTextureRect(left + texSize.x - left - right, 0, right, top);
	// Mid Left
	spriteList[3]->setTextureRect(0, top, left, texSize.y - top - bottom);
	// Mid Center
	spriteList[4]->setTextureRect(left, top, texSize.x - left - right, texSize.y - top - bottom);
	// Mid Right
	spriteList[5]->setTextureRect(left + texSize.x - left - right, top, right, texSize.y - top - bottom);
	// Bottom Left
	spriteList[6]->setTextureRect(0, top + texSize.y - top - bottom, left, bottom);
	// Bottom Center
	spriteList[7]->setTextureRect(left, top + texSize.y - top - bottom, texSize.x - left - right, bottom);
	// Bottom Right
	spriteList[8]->setTextureRect(left + texSize.x - left - right, top + texSize.y - top - bottom, right, bottom);

	
	setSize(sf::Vector2i(properties["size"]["x"].asInt(), properties["size"]["y"].asInt()));

	setColour(sf::Color(
		properties["colour"]["r"].asInt(), 
		properties["colour"]["g"].asInt(), 
		properties["colour"]["b"].asInt(), 
		properties["colour"]["a"].asInt()
		));

	setPos(sf::Vector2i(
		(int)(screenWidth * properties["pos"]["x"].asDouble()), 
		(int)(screenHeight * properties["pos"]["y"].asDouble())
		));
}

void NinePatch::setSize(const sf::Vector2i& s)
{	
	size = s;
	
	spriteList[0]->setSize(sf::Vector2i(left, top));
	spriteList[1]->setSize(sf::Vector2i(s.x - left - right, top));
	spriteList[2]->setSize(sf::Vector2i(right, top));

	spriteList[3]->setSize(sf::Vector2i(left, s.y - top - bottom));
	spriteList[4]->setSize(sf::Vector2i(s.x - left - right, s.y - top - bottom));
	spriteList[5]->setSize(sf::Vector2i(right, s.y - top - bottom));

	spriteList[6]->setSize(sf::Vector2i(left, bottom));
	spriteList[7]->setSize(sf::Vector2i(s.x - left - right, bottom));
	spriteList[8]->setSize(sf::Vector2i(right, bottom));

	// we need to change the pos of all the drawables after this is done
	setPos(pos);
}

void NinePatch::setPos(const sf::Vector2i& p)
{
	pos = p;

	spriteList[0]->setPos(sf::Vector2i(p.x - size.x / 2 , p.y - size.y / 2 + top));
	spriteList[1]->setPos(sf::Vector2i(p.x - size.x / 2 + left, p.y - size.y / 2 + top));
	spriteList[2]->setPos(sf::Vector2i(p.x + size.x / 2 - right, p.y - size.y / 2 + top));

	spriteList[3]->setPos(sf::Vector2i(p.x - size.x / 2 , p.y - size.y / 2 + top + bottom));
	spriteList[4]->setPos(sf::Vector2i(p.x - size.x / 2 + left, p.y - size.y / 2 + top + bottom));
	spriteList[5]->setPos(sf::Vector2i(p.x + size.x / 2 - right, p.y - size.y / 2 + top + bottom));

	spriteList[6]->setPos(sf::Vector2i(p.x - size.x / 2 , p.y + size.y / 2));
	spriteList[7]->setPos(sf::Vector2i(p.x - size.x / 2 + left, p.y + size.y / 2));
	spriteList[8]->setPos(sf::Vector2i(p.x + size.x / 2 - right, p.y + size.y / 2));
}

void NinePatch::setColour(const sf::Color& col)
{
	colour = col;
	
	for(size_t i = 0; i < spriteList.size(); ++i)
	{
		spriteList[i]->setColour(col);
	}
}

void NinePatch::simulation(sf::RenderWindow& window, float deltaT)
{

}

void NinePatch::eventReceiver(ABEvent* theEvent)
{
	
}

void NinePatch::render(sf::RenderWindow& window)
{
	for(size_t i = 0; i < spriteList.size(); ++i)
	{
		spriteList[i]->draw(window);
	}
}

void NinePatch::dispose()
{
	for(size_t i = 0; i < spriteList.size(); ++i)
	{
		spriteList[i]->dispose();
		delete spriteList[i];
	}
	spriteList.clear();
}

