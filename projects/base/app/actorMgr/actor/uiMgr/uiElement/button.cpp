#include "app\actorMgr\actor\uiMgr\uiElement\button.h"

#include "sfml\Graphics\Rect.hpp"

#include "app\eventMgr.h"

#include "app\eventMgr\event\mousePos.h"
#include "app\eventMgr\event\mousePressed.h"
#include "app\eventMgr\event\uiElementClicked.h"

#include <iostream>

Button::Button()
{
	mUiType = Ui::BUTTON;
}

Button::~Button()
{

}

void Button::setup(const Json::Value& properties)
{
	isHovering = false;
	wasPressed = false;
	isPressed = false;
	
	std::hash<std::string> hashFn;
	nameHash = hashFn(properties["name"].asString());
	
	mNinePatch.setup(properties["ninePatch"]);
	mLabel.setup(properties["label"]);
}

void Button::simulation(sf::RenderWindow& window, float deltaT)
{
	if(isPressed && !wasPressed)
	{
		wasPressed = true;
	}
	else if(!isPressed && wasPressed)
	{
		EventMgr::GetInstance()->addEvent(new UiElementClicked(mUiType, nameHash));
		wasPressed = false;
	}
	isPressed = false;
}

void Button::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::MOUSE_PRESSED)
	{
		MousePressed* button = static_cast<MousePressed*>(theEvent);
		if(button->getButton() == sf::Mouse::Left)
		{
			// see if the mouse was clicked within the bounds of the uiButton
			sf::Vector2i btnPos = mNinePatch.getPos();
			sf::Vector2i btnSize = mNinePatch.getSize();

			sf::Rect<int> rect(
				btnPos.x - btnSize.x / 2,
				btnPos.y - btnSize.y / 2,
				btnSize.x,
				btnSize.y
				);

			if(rect.contains(button->getPos()))
			{
				isPressed = true;
			}

		}
	}
	else if(theEvent->getEventType() == Event::MOUSE_POSITION)
	{
		MousePos* mousePos = static_cast<MousePos*>(theEvent);
			
		// see if the mouse is hovering within the bounds of the uiButton
		sf::Vector2i btnPos = mNinePatch.getPos();
		sf::Vector2i btnSize = mNinePatch.getSize();

		sf::Rect<int> rect(
			btnPos.x - btnSize.x / 2,
			btnPos.y - btnSize.y / 2,
			btnSize.x,
			btnSize.y
			);

		if(rect.contains(mousePos->getPos()))
		{
			if(!isHovering)
			{
				sf::Color c = mNinePatch.getColour();
				c.r -= 75;
				c.g -= 75;
				c.b -= 75;

				mNinePatch.setColour(c);
			}
			
			isHovering = true;
		}
		else
		{
			if(isHovering)
			{
				sf::Color c = mNinePatch.getColour();
				c.r += 75;
				c.g += 75;
				c.b += 75;

				mNinePatch.setColour(c);
			}
			
			isHovering = false;
		}
	}
}

void Button::render(sf::RenderWindow& window)
{
	mNinePatch.render(window);
	mLabel.render(window);
}

void Button::dispose()
{
	mNinePatch.dispose();
	mLabel.dispose();
}

