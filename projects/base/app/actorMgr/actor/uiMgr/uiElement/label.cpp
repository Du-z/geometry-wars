#include "app\actorMgr\actor\uiMgr\uiElement\label.h"

#include "app\eventMgr\event\mousePos.h"
#include "app\eventMgr\event\mousePressed.h"

#include "app\resourceMgr.h"
#include "general.h"

Label::Label()
{
	mUiType = Ui::LABEL;
}

Label::~Label()
{

}

void Label::setup(const Json::Value& properties)
{
	std::hash<std::string> hashFn;
	nameHash = hashFn(properties["name"].asString());
	
	sf::Font* font = ResourceMgr::GetInstance()->getFont(properties["fontDir"].asString());
	mText.setFont(*font);
	mText.setString(properties["string"].asString());
	mText.setCharacterSize(properties["size"].asInt());

	mText.setColor(sf::Color(
		properties["colour"]["r"].asInt(), 
		properties["colour"]["g"].asInt(), 
		properties["colour"]["b"].asInt(), 
		properties["colour"]["a"].asInt()
		));

	mText.setOrigin(mText.getLocalBounds().width / 2, mText.getLocalBounds().height / 2);

	mText.setPosition(
		(float)(screenWidth * properties["pos"]["x"].asDouble()), 
		(float)(screenHeight * properties["pos"]["y"].asDouble())
		);
}

void Label::setup(const std::string& name, const std::string& str, const std::string& fontDir,
	const sf::Vector2f& pos, const int charSize, const sf::Color& col)
{
	std::hash<std::string> hashFn;
	nameHash = hashFn(name);

	sf::Font* font = ResourceMgr::GetInstance()->getFont(fontDir);
	mText.setFont(*font);
	mText.setString(str);
	mText.setCharacterSize(charSize);

	mText.setColor(col);

	mText.setOrigin(mText.getLocalBounds().width / 2, mText.getLocalBounds().height / 2);

	mText.setPosition(pos);
}

void Label::simulation(sf::RenderWindow& window, float deltaT)
{
	
}

void Label::eventReceiver(ABEvent* theEvent)
{

}

void Label::render(sf::RenderWindow& window)
{
	window.draw(mText);
}

void Label::dispose()
{

}

