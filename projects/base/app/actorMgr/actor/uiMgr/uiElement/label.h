#ifndef H_LABEL_UI
#define H_LABEL_UI

#include "sfml\Graphics\Text.hpp"

#include "app\actorMgr\actor\uiMgr\uiElement.h"

class Label : public ABUiElement
{
public:
	Label();
	virtual ~Label();

	virtual void setup(const Json::Value& properties);

	void setup(const std::string& name, const std::string& str, const std::string& fontDir,
		const sf::Vector2f& pos, const int charSize, const sf::Color& col);

	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void setPos(const sf::Vector2f& pos){mText.setPosition(pos);}
	void setString(const std::string& str){mText.setString(str);}

private:
	sf::Text mText;
};

#endif // H_LABEL_UI