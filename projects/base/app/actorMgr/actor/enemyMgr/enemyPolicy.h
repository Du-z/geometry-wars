#ifndef H_ENEMY_POLICY
#define H_ENEMY_POLICY

#include "app\actorMgr\actor.h"

#include "app\drawable.h"
#include "Box2D\Box2D.h"

#include "app\actorMgr\actor\enemyMgr\enemyPolicy\abEnemy.h"

class EnemyPolicy : public ABActor
{
public:
	EnemyPolicy();
	virtual ~EnemyPolicy();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void launch(const b2Vec2& pos, Enemy::Type type, const std::vector<Drawable*>& sprites);

	bool getIsActive(){
		if(enemy == NULL)
			return false;
		else
			return true;
	}

private:

	ABEnemy* enemy;
	
	Drawable* pDrawable;
	b2Body* pBody;

	bool isDying;
	float deathTimer;
	bool killedByPlayer;
};

#endif // H_ENEMY_POLICY