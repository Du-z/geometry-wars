#ifndef H_ENEMY_ENUM
#define H_ENEMY_ENUM

namespace Enemy
{
	enum Type
	{
		UNSET = -1,
		WANDERER, // PURPLE PINWHEEL - Drifts around randomly
		GRUNT, // BLUE DIAMOND - Drifts toward player
		WEAVER, // GREEN SQUARE/DIAMOND - Drifts toward player, avoids bullets
		SNAKE, // SNAKE - Uses the same AI as the GRUNT
		GRAVITY_WELL, // RED CIRCLE - Sucks player towards itself
		COUNT
	};
}

#endif // H_ACTOR