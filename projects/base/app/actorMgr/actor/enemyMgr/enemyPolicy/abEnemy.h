#ifndef H_ABENEMY
#define H_ABENEMY

#include "Box2D\Box2D.h"
#include "sfml\Graphics\RenderWindow.hpp"
#include "app\drawable.h"

#include "app\actorMgr\actor\enemyMgr\enemyEnum.h"

class ABEnemy
{
public:
	ABEnemy();
	virtual ~ABEnemy();

	Enemy::Type getEnemyType(){return enemyType;}

	virtual void launch(b2Body* pBody) = 0;
	virtual void simulation(b2Body& pBody) = 0;
	virtual void draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody) = 0;

	// returns true the the enemy should die
	// internally does other stuff based off enemy type
	virtual bool gotHit() = 0;

protected:
	Enemy::Type enemyType; // the enum of the enemy

private:
};

#endif // H_ABENEMY