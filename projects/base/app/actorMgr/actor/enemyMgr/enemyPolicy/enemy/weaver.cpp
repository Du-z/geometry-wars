#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\weaver.h"

#include "general.h"
#include "app\soundMgr.h"

#include "app\screens\gameLoop.h"
#include "app\actorMgr\actor\player.h"

Weaver::Weaver()
{
	enemyType = Enemy::WEAVER;
}

Weaver::~Weaver()
{

}

void Weaver::launch(b2Body* pBody)
{
	SoundMgr::GetInstance()->play("data/sounds/enemy/weaverSpawn.ogg");

	/////////////// SETUP Box 2D /////////////////
	b2Vec2 size(4, 4); // Set size here

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.density = 0.1f;
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);

	//////////////////////////////////////////////
}

void Weaver::simulation(b2Body& pBody)
{
	float radian = 0;
	float speed = 15;

	// find the player and figure out the distance between
	b2Body* b = GameLoop::pB2World->GetBodyList();
	while (b != NULL)
	{
		ABActor* bodyData = static_cast<ABActor*>(b->GetUserData());
		if(bodyData->getActorType() == Actor::PLAYER)
		{
			radian = vectToRadian(b->GetPosition(), pBody.GetPosition());
			break;
		}
		b = b->GetNext();
	}

	// find bullets and figure out the distance between each
	// if they are too close the enemy runs away
	b = GameLoop::pB2World->GetBodyList();
	while (b != NULL)
	{
		ABActor* bodyData = static_cast<ABActor*>(b->GetUserData());
		if(bodyData->getActorType() == Actor::BULLET)
		{
			if(vectToDist(b->GetPosition(), pBody.GetPosition()) < 7.5f)
			{
				radian -= 2.f;
				speed = 20;
				break;
			}
		}
		b = b->GetNext();
	}

	b2Vec2 vec = speedRotToB2Vect(speed, radian);

	pBody.SetLinearVelocity(vec);
}

bool Weaver::gotHit()
{
	return true;
}

void Weaver::draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody)
{
	pDrawable.setPosRot(pBody);
	pDrawable.draw(window);
}