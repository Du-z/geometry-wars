#ifndef H_GRUNT_ENEMY
#define H_GRUNT_ENEMY

#include "app\actorMgr\actor\enemyMgr\enemyPolicy\abEnemy.h"

class Grunt : public ABEnemy
{
public:
	Grunt();
	virtual ~Grunt();

	virtual void launch(b2Body* pBody);
	virtual void simulation(b2Body& pBody);
	virtual void draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody);

	virtual bool gotHit();

private:
};

#endif // H_GRUNT_ENEMY