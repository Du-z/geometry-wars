#ifndef H_WANDERER_ENEMY
#define H_WANDERER_ENEMY

#include "app\actorMgr\actor\enemyMgr\enemyPolicy\abEnemy.h"

class Wanderer : public ABEnemy
{
public:
	Wanderer();
	virtual ~Wanderer();

	virtual void launch(b2Body* pBody);
	virtual void simulation(b2Body& pBody);
	virtual void draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody);

	virtual bool gotHit();

private:
};

#endif // H_WANDERER_ENEMY