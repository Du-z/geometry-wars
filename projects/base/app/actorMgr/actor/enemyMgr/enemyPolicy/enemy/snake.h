#ifndef H_SNAKE_ENEMY
#define H_SNAKE_ENEMY

#include "app\actorMgr\actor\enemyMgr\enemyPolicy\abEnemy.h"

class Snake : public ABEnemy
{
public:
	Snake();
	virtual ~Snake();

	virtual void launch(b2Body* pBody);
	virtual void simulation(b2Body& pBody);
	virtual void draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody);

	virtual bool gotHit();

	static Drawable* segDrawable;

private:

	static const int numSegments = 6;
	std::vector<b2Body*> pBodySegList;

};

#endif // H_SNAKE_ENEMY