#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\grunt.h"

#include "general.h"
#include "app\soundMgr.h"

#include "app\screens\gameLoop.h"
#include "app\actorMgr\actor\player.h"

Grunt::Grunt()
{
	enemyType = Enemy::GRUNT;
}

Grunt::~Grunt()
{

}

void Grunt::launch(b2Body* pBody)
{
	SoundMgr::GetInstance()->play("data/sounds/enemy/gruntSpawn.ogg");

	/////////////// SETUP Box 2D /////////////////
	b2Vec2 size(4, 4); // Set size here

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.density = 0.1f;
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);

	//////////////////////////////////////////////
}

void Grunt::simulation(b2Body& pBody)
{
	float radian = 0;
	float speed = 11;

	// find the player and figure out the distance between
	b2Body* b = GameLoop::pB2World->GetBodyList();
	while (b != NULL)
	{
		ABActor* bodyData = static_cast<ABActor*>(b->GetUserData());
		if(bodyData->getActorType() == Actor::PLAYER)
		{
			radian = vectToRadian(b->GetPosition(), pBody.GetPosition());
			break;
		}
		b = b->GetNext();
	}	

	b2Vec2 vec = speedRotToB2Vect(speed, radian);

	pBody.SetLinearVelocity(vec);
}

bool Grunt::gotHit()
{
	return true;
}

void Grunt::draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody)
{
	pDrawable.setPosRot(pBody);
	pDrawable.draw(window);
}