#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\gravityWell.h"

#include "general.h"
#include "app\soundMgr.h"

#include "app\screens\gameLoop.h"
#include "app\actorMgr\actor\player.h"

GravityWell::GravityWell()
{
	enemyType = Enemy::GRAVITY_WELL;
}

GravityWell::~GravityWell()
{
	if(suckSound != -1)
	{
		SoundMgr::GetInstance()->stop(suckSound);
	}
}

void GravityWell::launch(b2Body* pBody)
{
	SoundMgr::GetInstance()->play("data/sounds/enemy/gravityWellSpawn.ogg");
	suckSound = -1;

	isSucking = false;
	hitCount = 0;

	/////////////// SETUP Box 2D /////////////////
	b2Vec2 size(6, 6); // Set size here

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.density = 0.1f;
	fixtureDef.isSensor = true;
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);

	//////////////////////////////////////////////
}

void GravityWell::simulation(b2Body& pBody)
{
	//pBody.SetLinearVelocity(b2Vec2(0,0));
	//pBody.SetAngularVelocity(0);

	if(isSucking)
	{
		float pullDist = 0;

		// find the player and figure out the distance between
		b2Body* b = GameLoop::pB2World->GetBodyList();
		while (b != NULL)
		{
			ABActor* bodyData = static_cast<ABActor*>(b->GetUserData());
			if(bodyData->getActorType() == Actor::PLAYER)
			{
				float dist = vectToDist(b->GetPosition(), pBody.GetPosition());
				pullDist = 30;

				if(dist < pullDist)
				{
					float rot = vectToRadian(b->GetPosition(), pBody.GetPosition());
					float pullStrength = -4.25f * (pullDist - dist) / pullDist;
				
					b->ApplyLinearImpulse(speedRotToB2Vect(pullStrength, rot), b->GetLocalCenter());
				}

				
			}
			else if(bodyData->getActorType() == Actor::BULLET)
			{
				float dist = vectToDist(b->GetPosition(), pBody.GetPosition());
				pullDist = 20;

				if(dist < pullDist)
				{
					float rot = vectToRadian(b->GetPosition(), pBody.GetPosition());
					float pullStrength = 3.5f * (pullDist - dist) / pullDist;

					b->ApplyLinearImpulse(speedRotToB2Vect(pullStrength, rot), b->GetLocalCenter());
				}


			}

			b = b->GetNext();
		}	
	}
}

bool GravityWell::gotHit()
{
	hitCount++;
	
	if(isSucking)
	{
		if(hitCount > 4)
		{
			SoundMgr::GetInstance()->stop(suckSound);
			suckSound = -1;
			return true;
		}
	}
	else
	{
		isSucking = true;

		suckSound = SoundMgr::GetInstance()->play("data/sounds/enemy/gravityWellSuck.ogg");
		SoundMgr::GetInstance()->setLoop(suckSound, true);
	}

	SoundMgr::GetInstance()->play("data/sounds/enemy/gravityWellHit.ogg");
	return false;
}

void GravityWell::draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody)
{
	pDrawable.setPosRot(pBody);
	pDrawable.draw(window);
}