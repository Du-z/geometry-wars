#ifndef H_WEAVER_ENEMY
#define H_WEAVER_ENEMY

#include "app\actorMgr\actor\enemyMgr\enemyPolicy\abEnemy.h"

class Weaver : public ABEnemy
{
public:
	Weaver();
	virtual ~Weaver();

	virtual void launch(b2Body* pBody);
	virtual void simulation(b2Body& pBody);
	virtual void draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody);

	virtual bool gotHit();

private:
};

#endif // H_WEAVER_ENEMY