#ifndef H_GRAVITY_WELL_ENEMY
#define H_GRAVITY_WELL_ENEMY

#include "app\actorMgr\actor\enemyMgr\enemyPolicy\abEnemy.h"

class GravityWell : public ABEnemy
{
public:
	GravityWell();
	virtual ~GravityWell();

	virtual void launch(b2Body* pBody);
	virtual void simulation(b2Body& pBody);
	virtual void draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody);

	virtual bool gotHit();

private:

	bool isSucking;
	int hitCount;

	int suckSound;
};

#endif // H_GRAVITY_WELL_ENEMY