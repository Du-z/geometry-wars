#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\wanderer.h"

#include "general.h"
#include "app\screens\gameLoop.h"
#include "app\soundMgr.h"

Wanderer::Wanderer()
{
	enemyType = Enemy::WANDERER;
}

Wanderer::~Wanderer()
{

}

void Wanderer::launch(b2Body* pBody)
{
	SoundMgr::GetInstance()->play("data/sounds/enemy/wandererSpawn.ogg");

	/////////////// SETUP Box 2D /////////////////
	b2Vec2 size(4, 4); // Set size here

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.density = 0.1f;
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);

	//////////////////////////////////////////////
}

void Wanderer::simulation(b2Body& pBody)
{
	float speed = 10;

	if(vectToSpeed(pBody.GetLinearVelocity()) < speed)
	{
		b2Vec2 vec = speedRotToB2Vect(speed, randFloat(-3.14159265f, 3.14159265f));

		pBody.SetLinearVelocity(vec);
	}
	else if(randInt(0, 150) == 0)
	{
		float currAngle = vectToRadian(pBody.GetLinearVelocity());

		b2Vec2 vec = speedRotToB2Vect(speed, currAngle + randFloat(-0.5f, 0.5f));

		pBody.SetLinearVelocity(vec);
	}
}

bool Wanderer::gotHit()
{
	return true;
}

void Wanderer::draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody)
{
	pDrawable.setPosRot(pBody);
	pDrawable.draw(window);
}