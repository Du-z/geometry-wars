#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\snake.h"

#include "general.h"
#include "app\screens\gameLoop.h"
#include "app\soundMgr.h"

Drawable* Snake::segDrawable = NULL;

Snake::Snake()
{
	enemyType = Enemy::SNAKE;
}

Snake::~Snake()
{
	for(int i = 0; i < numSegments; ++i)
	{
		GameLoop::pB2World->DestroyBody(pBodySegList[i]);
		pBodySegList[i] = NULL;
	}
}

void Snake::launch(b2Body* pBody)
{
	SoundMgr::GetInstance()->play("data/sounds/enemy/snakeSpawn.ogg");


	/////////////// SETUP Box 2D /////////////////
	b2Vec2 size(4, 4); // Set size here

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 3, size.y / 3, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.density = 0.0001f;
	fixtureDef.isSensor = true;
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);

	// create the body segments
	b2BodyDef bodyDef;
	bodyDef.position.Set(pBody->GetPosition().x, pBody->GetPosition().y);
	bodyDef.type = b2_dynamicBody;
	bodyDef.userData = pBody->GetUserData();

	pBodySegList.resize(numSegments);

	for(int i = 0; i < numSegments; ++i)
	{
		pBodySegList[i] = GameLoop::pB2World->CreateBody(&bodyDef);
		pBodySegList[i]->CreateFixture(&fixtureDef);

		b2DistanceJointDef jointDef;
		jointDef.length = 0.1f;
		if(i == 0)
		{
			jointDef.bodyA = pBody;
		}
		else
		{
			jointDef.bodyA = pBodySegList[i - 1];
		}

		jointDef.bodyB = pBodySegList[i];
		jointDef.localAnchorA.Set(-1,0);
		jointDef.localAnchorB.Set(1,0);
		jointDef.dampingRatio = 1.f;
		jointDef.frequencyHz = 20.0f;

		GameLoop::pB2World->CreateJoint(&jointDef);
	}

	//////////////////////////////////////////////
}

void Snake::simulation(b2Body& pBody)
{
	float radian = 0;
	float speed = 11;

	// find the player and figure out the distance between
	b2Body* b = GameLoop::pB2World->GetBodyList();
	while (b != NULL)
	{
		ABActor* bodyData = static_cast<ABActor*>(b->GetUserData());
		if(bodyData->getActorType() == Actor::PLAYER)
		{
			radian = vectToRadian(b->GetPosition(), pBody.GetPosition());
			break;
		}
		b = b->GetNext();
	}	

	b2Vec2 vec = speedRotToB2Vect(speed, radian);

	pBody.SetTransform(pBody.GetPosition(), radian);
	pBody.SetLinearVelocity(vec);

	// give drag to the tail segments
	for(int i = 0; i < numSegments; ++i)
	{
		b2Vec2 vec = pBodySegList[i]->GetLinearVelocity();

		float worldFriction = 0.1f;

		if(vec.x - worldFriction > 0)
			vec.x -= worldFriction;
		else if(vec.x + worldFriction < 0)
			vec.x += worldFriction;
		else
			vec.x = 0;

		if(vec.y - worldFriction > 0)
			vec.y -= worldFriction;
		else if(vec.y + worldFriction < 0)
			vec.y += worldFriction;
		else
			vec.y = 0;

		pBodySegList[i]->SetLinearVelocity(vec);
	}

	
}

bool Snake::gotHit()
{
	return true;
}

void Snake::draw(sf::RenderWindow& window, Drawable& pDrawable, b2Body& pBody)
{
	pDrawable.setPosRot(pBody);
	pDrawable.draw(window);

	for(int i = 0; i < numSegments; ++i)
	{
		segDrawable->setPosRot(*pBodySegList[i]);
		segDrawable->draw(window);
	}
}