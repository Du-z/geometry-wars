#include "app\actorMgr\actor\enemyMgr\enemyPolicy.h"

#include <iostream>

#include "app\screens\gameLoop.h"

#include "app\soundMgr.h"

#include "app\eventMgr.h"
#include "app\eventMgr\event\collision.h"
#include "app\eventMgr\event\playerKilled.h"
#include "app\eventMgr\event\enemyKilled.h"
#include "app\eventMgr\event\dropBomb.h"

#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\grunt.h"
#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\wanderer.h"
#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\weaver.h"
#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\snake.h"
#include "app\actorMgr\actor\enemyMgr\enemyPolicy\enemy\gravityWell.h"

EnemyPolicy::EnemyPolicy()
{
	mActorType = Actor::ENEMY;

	enemy = NULL;
}

EnemyPolicy::~EnemyPolicy()
{
}

void EnemyPolicy::setup(b2World* pB2World, const Json::Value& properties)
{
	isDying = false;
	deathTimer = 0;

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = NULL;
	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::B2D_COLLISION, this);
	EventMgr::GetInstance()->addSubscriber(Event::PLAYER_KILLED, this);
	EventMgr::GetInstance()->addSubscriber(Event::DROP_BOMB, this);
	//////////////////////////////////////////////

	pBody = NULL;
}

void EnemyPolicy::launch(const b2Vec2& pos, Enemy::Type type, const std::vector<Drawable*>& sprites)
{
	isDying = false;
	deathTimer = 0;

	pDrawable = sprites[type];

	ABEnemy* e;

	switch (type)
	{
	case Enemy::WANDERER:
		e = new Wanderer();
		break;
	case Enemy::GRUNT:
		e = new Grunt();
		break;
	case Enemy::WEAVER:
		e = new Weaver();
		break;
	case Enemy::SNAKE:
		e = new Snake();
		break;
	case Enemy::GRAVITY_WELL:
		e = new GravityWell();
		break;
	default:
		break;
	}

	b2BodyDef bodyDef;
	bodyDef.position = pos;
	bodyDef.type = b2_dynamicBody;
	bodyDef.fixedRotation = true;
	pBody = GameLoop::pB2World->CreateBody(&bodyDef);
	pBody->SetUserData(this);

	e->launch(pBody);
	enemy = e;
}

void EnemyPolicy::simulation(sf::RenderWindow& window, float deltaT)
{
	if(enemy != NULL && pBody != NULL)
	{
		enemy->simulation(*pBody);
		
		if(isDying)
		{
			deathTimer -= deltaT;
			if(deathTimer < 0)
			{
				EventMgr::GetInstance()->addEvent(new EnemyKilled(pBody->GetPosition(), enemy->getEnemyType(), killedByPlayer));
			
				GameLoop::pB2World->DestroyBody(pBody);
				pBody = NULL;
				
				delete enemy;
				enemy = NULL;
			}
		}
	}
}

void EnemyPolicy::eventReceiver(ABEvent* theEvent)
{
	if(enemy != NULL && pBody != NULL)
	{
		if(theEvent->getEventType() == Event::B2D_COLLISION)
		{
			Collision* collision = static_cast<Collision*>(theEvent);

			if(!isDying && collision->getOtherBody()->getActorType() == Actor::BULLET && enemy->gotHit())
			{
				isDying = true;
				killedByPlayer = true;
				deathTimer = 0;

				SoundMgr::GetInstance()->play("data/sounds/enemy/explode.ogg");
			}
		}
		else if(theEvent->getEventType() == Event::PLAYER_KILLED)
		{
			PlayerKilled* playerKilled = static_cast<PlayerKilled*>(theEvent);

			isDying = true;
			killedByPlayer = false;
			deathTimer = vectToDist(playerKilled->getPos(), pBody->GetPosition()) / 50.f;
		
		}
		else if(theEvent->getEventType() == Event::DROP_BOMB)
		{
			DropBomb* dropBomb = static_cast<DropBomb*>(theEvent);

			isDying = true;
			killedByPlayer = true;
			deathTimer = vectToDist(dropBomb->getPos(), pBody->GetPosition()) / 50.f;
		}
	}
}

void EnemyPolicy::render(sf::RenderWindow& window)
{
	if(enemy != NULL && pBody != NULL)
	{
		enemy->draw(window, *pDrawable, *pBody);
	}
}

void EnemyPolicy::dispose()
{
	// the manager owns the drawable
	//delete pDrawable;
	//pDrawable = NULL;

	if(pBody != NULL)
	{
		pBody->GetWorld()->DestroyBody(pBody);
		pBody = NULL;
	}
}
