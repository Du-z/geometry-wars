#include "app\actorMgr\actor\wall.h"

#include "app\eventMgr\event\collision.h"

Wall::Wall()
{
	mActorType = Actor::WALL;
}

Wall::~Wall()
{

}

void Wall::setup(b2World* pB2World, const Json::Value& properties)
{

	/////////////// SETUP GENERAL ////////////////

	//////////////////////////////////////////////

	/////////////// SETUP Box 2D /////////////////
	b2Vec2 size(mapWidth / worldScale, 0.25f); // Set size here

	b2BodyDef bodyDef;

	bodyDef.position.Set(
		properties["pos"]["x"].asDouble() + size.x / 2,
		properties["pos"]["y"].asDouble() + size.y / 2
		);

	bodyDef.angle = properties["rot"].asDouble() / 180 * b2_pi;
	bodyDef.type = b2_staticBody;
	bodyDef.fixedRotation = true;
	pBody = pB2World->CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0.7f; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.0f; // set the elasticity of the obj
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);
	pBody->SetUserData(this);
	//////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/sprites.png");
	pDrawable->setTextureRect(0, 0, 1, 1);
	pDrawable->setSize(size);
	pDrawable->setOrigin(sf::Vector2f((float) pDrawable->getSprite()->getTextureRect().width / 2, (float) pDrawable->getSprite()->getTextureRect().height / 2));
	pDrawable->setColour(sf::Color::White);
	pDrawable->setPosRot(*pBody);
	//////////////////////////////////////////////

	/////////////// SETUP Sound /////////////////

	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////

	//////////////////////////////////////////////
}

void Wall::simulation(sf::RenderWindow& window, float deltaT)
{
	
}

void Wall::eventReceiver(ABEvent* theEvent)
{
	
}

void Wall::render(sf::RenderWindow& window)
{
	pDrawable->draw(window);
}

void Wall::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;

	pBody->GetWorld()->DestroyBody(pBody);
	pBody = NULL;
}
