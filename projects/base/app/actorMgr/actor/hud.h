#ifndef H_HUD_ACTOR
#define H_HUD_ACTOR

#include <deque>

#include "app\actorMgr\actor.h"

#include "app\actorMgr\actor\uiMgr\uiElement\label.h"

struct pointLblPod
{
	sf::Vector2f pos;
	float deathTime;
	std::string points;
};

class Hud : public ABActor
{
public:
	Hud();
	virtual ~Hud();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	Label scoreLbl;
	Label livesLbl;
	Label bombsLbl;

	Label pointLbl;
	std::deque<pointLblPod> pointLblList;

	int score, lives, bombs;
};

#endif // H_HUD_ACTOR