#include "app\actorMgr\actor\particleMgr.h"

#include "kf\kf_log.h"

#include "app\eventMgr.h"
#include "app\resourceMgr.h"

#include "app\eventMgr\event\enemyKilled.h"
#include "app\eventMgr\event\playerKilled.h"
#include "app\eventMgr\event\playerMove.h"
#include "app\eventMgr\event\dropBomb.h"
#include "app\eventMgr\event\wallHit.h"

ParticleMgr::ParticleMgr()
{
	mActorType = Actor::PARTICLE_MGR;
}

ParticleMgr::~ParticleMgr()
{

}

void ParticleMgr::setup(b2World* pB2World, const Json::Value& properties)
{
	Json::Value& particles = *ResourceMgr::GetInstance()->getJson("data/config/particleTypes.json");

	// quickly calculate how many emitters will be needed
	totalEmitters = 0;
	for(size_t i = 0; i < particles["particles"].size(); ++i)
	{
		totalEmitters += particles["particles"][i]["emitterCount"].asInt();
	}
	
	emitterList.resize(totalEmitters);
	
	for(size_t i = 0, k = 0; i < particles["particles"].size(); ++i)
	{
		size_t numEmitters = particles["particles"][i]["emitterCount"].asInt();
		for(size_t j = 0; j < numEmitters; ++j, ++k)
		{
			emitterList[k].setup(particles["particles"][i]);
		}
	}

	// we don't need the json anymore
	ResourceMgr::GetInstance()->destroyJson("data/config/particleTypes.json");

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::ENEMY_KILLED, this);
	EventMgr::GetInstance()->addSubscriber(Event::PLAYER_KILLED, this);
	EventMgr::GetInstance()->addSubscriber(Event::PLAYER_MOVE, this);
	EventMgr::GetInstance()->addSubscriber(Event::DROP_BOMB, this);
	EventMgr::GetInstance()->addSubscriber(Event::WALL_HIT, this);
	//////////////////////////////////////////////
}

void ParticleMgr::simulation(sf::RenderWindow& window, float deltaT)
{
	for(size_t i = 0; i < totalEmitters; ++i)
	{
		emitterList[i].simulation(deltaT);
	}
}

void ParticleMgr::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::WALL_HIT)
	{
		WallHit* wallHit = static_cast<WallHit*>(theEvent);

		std::hash<std::string> hashFn;
		size_t hash = hashFn("bulletExplosion");

		for(size_t i = 0; i < totalEmitters; ++i)
		{
			if(emitterList[i].getIsActive() == false && emitterList[i].getNameHash() == hash)
			{
				emitterList[i].start(wallHit->getPos(), 0);
				return;
			}
		}
		kf_log("Particle Mgr has run out of 'bulletExplosion' particles.");
	}
	else if(theEvent->getEventType() == Event::ENEMY_KILLED)
	{
		EnemyKilled* enemyKilled = static_cast<EnemyKilled*>(theEvent);
	
		std::hash<std::string> hashFn;
		size_t hash = hashFn("explosion");

		for(size_t i = 0; i < totalEmitters; ++i)
		{
			if(emitterList[i].getIsActive() == false && emitterList[i].getNameHash() == hash)
			{
				emitterList[i].start(enemyKilled->getPos(), 0);
				return;
			}
		}
		kf_log("Particle Mgr has run out of 'explosion' particles.");
	}
	else if(theEvent->getEventType() == Event::PLAYER_KILLED)
	{
		PlayerKilled* playerKilled = static_cast<PlayerKilled*>(theEvent);
	
		std::hash<std::string> hashFn;
		size_t hash = hashFn("playerExplosion");

		for(size_t i = 0; i < totalEmitters; ++i)
		{
			if(emitterList[i].getNameHash() == hash)
			{
				emitterList[i].start(playerKilled->getPos(), 0);
				return;
			}
		}
		kf_log("Particle Mgr could not find the 'playerExplosion' particle.");
	}
	else if(theEvent->getEventType() == Event::DROP_BOMB)
	{
		DropBomb* dropBomb = static_cast<DropBomb*>(theEvent);
	
		std::hash<std::string> hashFn;
		size_t hash = hashFn("bombExplosion");

		for(size_t i = 0; i < totalEmitters; ++i)
		{
			if(emitterList[i].getNameHash() == hash)
			{
				emitterList[i].start(dropBomb->getPos(), 0);
				return;
			}
		}
		kf_log("Particle Mgr could not find the 'bombExplosion' particle.");
	}
	else if(theEvent->getEventType() == Event::PLAYER_MOVE)
	{
		PlayerMove* playerMove = static_cast<PlayerMove*>(theEvent);
		for(size_t i = 0; i < totalEmitters; ++i)
		{
			std::hash<std::string> hashFn;
			size_t hash = hashFn("sparkTrail");
			
			if(emitterList[i].getNameHash() == hash)
			{
				
				if(playerMove->getHasMoved())
				{
					if(!emitterList[i].getIsActive())
					{
						emitterList[i].start(playerMove->getPos(), 0);
					}
					else
					{
						emitterList[i].setPos(playerMove->getPos());
						emitterList[i].setAngleDirection(0);
					}
				}
				else
				{
					emitterList[i].stop();
				}

				return;
			}

			kf_log("Particle Mgr could not find the 'sparkTrail' particle.");
		}
	}
}

void ParticleMgr::render(sf::RenderWindow& window)
{
	for(size_t i = 0; i < totalEmitters; ++i)
	{
		emitterList[i].render(window);
	}
}

void ParticleMgr::dispose()
{
	for(size_t i = 0; i < totalEmitters; ++i)
	{
		emitterList[i].dispose();
	}

	emitterList.clear();
}
