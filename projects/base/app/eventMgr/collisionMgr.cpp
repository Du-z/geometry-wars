#include "app\eventMgr\collisionMgr.h"

#include "app\actorMgr\actor.h"

#include "app\eventMgr\event\collision.h"
#include "app\eventMgr\eventEnum.h"

void CollisionMgr::BeginContact(b2Contact* contact)
{
	ABActor* bodyA = static_cast<ABActor*>(contact->GetFixtureA()->GetBody()->GetUserData());
	ABActor* bodyB = static_cast<ABActor*>(contact->GetFixtureB()->GetBody()->GetUserData());

	bodyA->eventReceiver(&Collision(bodyB));
	bodyB->eventReceiver(&Collision(bodyA));
}

void CollisionMgr::EndContact(b2Contact* contact)
{
}

void CollisionMgr::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
}

void CollisionMgr::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{
}
