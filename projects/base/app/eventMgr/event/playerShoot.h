#ifndef H_PLAYER_SHOOT
#define H_PLAYER_SHOOT

#include "app\eventMgr\event.h"

#include "Box2D\Common\b2Math.h"

class PlayerShoot : public ABEvent
{
public:
	PlayerShoot(const b2Vec2& pos, const float radian);
	virtual ~PlayerShoot();

	b2Vec2 getPos(){return mPos;}
	float getRadian(){return mRadian;}

private:

	b2Vec2 mPos;
	float mRadian;
};

#endif // H_PLAYER_SHOOT