#include "app\eventMgr\event\enemyKilled.h"

EnemyKilled::EnemyKilled(const b2Vec2& pos, Enemy::Type type, bool killedByP)
{
	mEventType = Event::ENEMY_KILLED;

	mPos = pos;
	enemyType = type;
	killedByPlayer = killedByP;
}

EnemyKilled::~EnemyKilled()
{

}