#include "app\eventMgr\event\playerKilled.h"

PlayerKilled::PlayerKilled(const b2Vec2& pos, const int lives)
{
	mEventType = Event::PLAYER_KILLED;

	livesLeft = lives;
	mPos = pos;
}

PlayerKilled::~PlayerKilled()
{

}