#include "app\eventMgr\event\dropBomb.h"

DropBomb::DropBomb(const b2Vec2& pos, const int bombsLeft)
{
	mEventType = Event::DROP_BOMB;

	mPos = pos;
	bombs = bombsLeft;
}

DropBomb::~DropBomb()
{

}