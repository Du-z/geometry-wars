#ifndef H_MOUSE_PRESSED
#define H_MOUSE_PRESSED

#include "app\eventMgr\event.h"

#include "SFML\Window\Mouse.hpp"

class MousePressed : public ABEvent
{
public:
	MousePressed(const sf::Mouse::Button button, const sf::Vector2i& pos);
	MousePressed(const sf::Mouse::Button button, const sf::Vector2f& pos);
	virtual ~MousePressed();

	sf::Mouse::Button getButton(){return mButton;}
	sf::Vector2i getPos(){return mPos;}

private:

	sf::Mouse::Button mButton;
	sf::Vector2i mPos;
};

#endif // H_MOUSE_PRESSED