#ifndef H_PLAYER_KILLED
#define H_PLAYER_KILLED

#include "app\eventMgr\event.h"

#include "Box2D\Common\b2Math.h"

class PlayerKilled : public ABEvent
{
public:
	PlayerKilled(const b2Vec2& pos, const int lives);
	virtual ~PlayerKilled();

	int getLives(){return livesLeft;}
	b2Vec2 getPos(){return mPos;}

private:

	b2Vec2 mPos;
	int livesLeft;
};

#endif // H_PLAYER_KILLED