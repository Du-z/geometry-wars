#ifndef H_KEY_PRESSED
#define H_KEY_PRESSED

#include "app\eventMgr\event.h"

#include "SFML\Window\Keyboard.hpp"

class KeyPressed : public ABEvent
{
public:
	explicit KeyPressed(const sf::Keyboard::Key key);
	virtual ~KeyPressed();

	sf::Keyboard::Key getKey(){return mKey;}

private:

	sf::Keyboard::Key mKey;
};

#endif // H_KEY_PRESSED