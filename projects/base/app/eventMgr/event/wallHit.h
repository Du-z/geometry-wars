#ifndef H_WALL_HIT
#define H_WALL_HIT

#include "app\eventMgr\event.h"

#include "Box2D\Common\b2Math.h"

class WallHit : public ABEvent
{
public:
	explicit WallHit(const b2Vec2& pos);
	virtual ~WallHit();

	b2Vec2 getPos(){return mPos;}

private:

	b2Vec2 mPos;
};

#endif // H_PLAYER_SHOOT