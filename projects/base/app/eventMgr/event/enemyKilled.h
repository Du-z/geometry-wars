#ifndef H_ENEMY_KILLED
#define H_ENEMY_KILLED


#include "app\eventMgr\event.h"

#include "Box2D\Common\b2Math.h"

#include "app\actorMgr\actor\enemyMgr\enemyEnum.h"

class EnemyKilled : public ABEvent
{
public:
	EnemyKilled(const b2Vec2& pos, Enemy::Type type, bool killedByPlayer);
	virtual ~EnemyKilled();

	int getEnemyType(){return enemyType;}
	bool getKilledByPlayer(){return killedByPlayer;}
	b2Vec2 getPos(){return mPos;}

private:

	b2Vec2 mPos;
	Enemy::Type enemyType;
	bool killedByPlayer;
};

#endif // H_ENEMY_KILLED