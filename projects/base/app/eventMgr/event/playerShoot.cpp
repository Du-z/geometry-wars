#include "app\eventMgr\event\playerShoot.h"

PlayerShoot::PlayerShoot(const b2Vec2& pos, const float radian)
{
	mEventType = Event::PLAYER_SHOOT;

	mPos = pos;
	mRadian = radian;
}

PlayerShoot::~PlayerShoot()
{

}