#include "app\eventMgr\event\keyPressed.h"

KeyPressed::KeyPressed(const sf::Keyboard::Key key)
{
	mEventType = Event::KEY_PRESSED;

	mKey = key;
}

KeyPressed::~KeyPressed()
{

}