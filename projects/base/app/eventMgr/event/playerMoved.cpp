#include "app\eventMgr\event\playerMove.h"

PlayerMove::PlayerMove(b2Vec2 pos, float rot, bool hasMoved)
{
	mEventType = Event::PLAYER_MOVE;

	mHasMoved = hasMoved;
	mPos = pos;
	mRot = rot;
}

PlayerMove::~PlayerMove()
{

}