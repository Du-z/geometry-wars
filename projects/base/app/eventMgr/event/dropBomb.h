#ifndef H_DROP_BOMB
#define H_DROP_BOMB

#include "app\eventMgr\event.h"

#include "Box2D\Common\b2Math.h"

class DropBomb : public ABEvent
{
public:
	explicit DropBomb(const b2Vec2& pos, const int bombsLeft);
	virtual ~DropBomb();

	b2Vec2 getPos(){return mPos;}
	int getBombs(){return bombs;}

private:

	b2Vec2 mPos;
	int bombs;
};

#endif // H_DROP_BOMB