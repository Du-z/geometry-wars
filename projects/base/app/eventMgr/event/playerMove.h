#ifndef H_PLAYER_MOVED
#define H_PLAYER_MOVED

#include "app\eventMgr\event.h"

#include "Box2D\Common\b2Math.h"

class PlayerMove : public ABEvent
{
public:
	PlayerMove(b2Vec2 pos, float rot, bool hasMoved);
	virtual ~PlayerMove();

	b2Vec2 getPos(){return mPos;}
	float getRot(){return mRot;}
	bool getHasMoved(){return mHasMoved;};

private:

	bool mHasMoved;
	b2Vec2 mPos;
	float mRot;
};

#endif // H_PLAYER_MOVED