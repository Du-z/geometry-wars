#include "app\eventMgr.h"

EventMgr* EventMgr::mInstance = NULL;
EventMgr* EventMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new EventMgr();

	return mInstance;
}

EventMgr::EventMgr()
{
	subscriberList.resize(Event::COUNT);
}

EventMgr::~EventMgr()
{
	mInstance = NULL;
}

void EventMgr::notifySubscribers(ABScreen& screen)
{

	// go through each new event
	for(size_t i1 = 0; i1 < eventList.size(); ++i1)
	{
		Event::Types eventType = eventList[i1]->getEventType();
		// go through all the actors subscribed to that event
		for(size_t i2 = 0, size = subscriberList[eventType].size(); i2 < size; ++i2)
		{
			subscriberList[eventType][i2]->eventReceiver(eventList[i1]);
		}
		screen.eventReceiver(eventList[i1]);
		delete eventList[i1];
	}
	eventList.clear();
}

void EventMgr::addSubscriber(Event::Types eventType, ABActor* actor)
{
	subscriberList[eventType].push_back(actor);
}

void EventMgr::clearSubscribers()
{
	for(size_t i1 = 0; i1 < Event::COUNT; ++i1)
	{
		subscriberList[i1].clear();
	}

}

void EventMgr::addEvent(ABEvent* theEvent)
{
	eventList.push_back(theEvent);
}