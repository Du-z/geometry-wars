#include "app\resourceMgr.h"

#include <fstream>
#include "kf\kf_log.h"

#include "json/reader.h"

ResourceMgr* ResourceMgr::mInstance = NULL;

ResourceMgr* ResourceMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new ResourceMgr();

	return mInstance;
}

ResourceMgr::ResourceMgr()
{

}

ResourceMgr::~ResourceMgr()
{
	mInstance = NULL;
}

sf::Texture* ResourceMgr::getTexture(const std::string& dir)
{
	if (textureList.find(dir) == textureList.end())
	{
		// It doesn't exists! load it!
		std::vector<char>* data = load(dir);

		sf::Texture* tex = new sf::Texture();

		if(data == NULL)
		{
			// The data is bad
			sf::RenderTexture renderTex;
			renderTex.create(1, 1);
			renderTex.clear(sf::Color::Magenta);

			*tex = renderTex.getTexture();
		}
		else
		{
			// The data is good
			tex->loadFromMemory(&data->front(), data->size(), sf::IntRect());
		}

		delete data; // delete the loaded data, it isn't needed anymore

		textureList[dir] = tex; // add it to the map

		return textureList[dir];
	}
	else
	{
		// It exists!
		return textureList[dir];
	}
}

bool ResourceMgr::destroyTexture(const std::string& dir)
{
	std::map<std::string, sf::Texture*>::iterator it = textureList.find(dir);

	if (it != textureList.end()) {
		delete it->second;
		textureList.erase(it);

		return true;
	} 
	else

		return false;
}

void ResourceMgr::destroyAllTextures()
{
	std::map<std::string, sf::Texture*>::iterator iter;
	for (iter = textureList.begin(); iter != textureList.end(); ++iter)
	{
		delete iter->second;
	}

	textureList.clear();
}

sf::Font* ResourceMgr::getFont(const std::string& dir)
{
	if (fontList.find(dir) == fontList.end())
	{
		// It doesn't exists! load it!
		std::vector<char>* data = load(dir);

		sf::Font* font = new sf::Font();

		if(data == NULL)
		{
			// The data is bad
			return NULL;
		}
		else
		{
			// The data is good
			font->loadFromMemory(&data->front(), data->size());
		}

		//delete data; // delete the loaded data, it isn't needed anymore

		fontList[dir] = font; // add it to the map

		return fontList[dir];
	}
	else
	{
		// It exists!
		return fontList[dir];
	}
}

bool ResourceMgr::destroyFont(const std::string& dir)
{
	std::map<std::string, sf::Font*>::iterator it = fontList.find(dir);

	if (it != fontList.end()) {
		delete it->second;
		fontList.erase(it);

		return true;
	}

	return false;
}

void ResourceMgr::destroyAllFonts()
{
	std::map<std::string, sf::Font*>::iterator iter;
	for (iter = fontList.begin(); iter != fontList.end(); ++iter)
	{
		delete iter->second;
	}

	fontList.clear();
}

sf::SoundBuffer* ResourceMgr::getSound(const std::string& dir)
{
	if (soundList.find(dir) == soundList.end())
	{
		// It doesn't exists! load it!
		std::vector<char>* data = load(dir);

		sf::SoundBuffer* sound = new sf::SoundBuffer();

		if(data == NULL)
		{
			// The data is bad
			return NULL;
		}
		else
		{
			// The data is good
			sound->loadFromMemory(&data->front(), data->size());
		}

		delete data; // delete the loaded data, it isn't needed anymore
		soundList[dir] = sound; // add it to the map

		return soundList[dir];
	}
	else
	{
		// It exists!
		return soundList[dir];
	}
}

bool ResourceMgr::destroySound(const std::string& dir)
{
	std::map<std::string, sf::SoundBuffer*>::iterator it = soundList.find(dir);

	if (it != soundList.end()) {
		delete it->second;
		soundList.erase(it);

		return true;
	}

	return false;
}

void ResourceMgr::destroyAllSounds()
{
	std::map<std::string, sf::SoundBuffer*>::iterator iter;
	for (iter = soundList.begin(); iter != soundList.end(); ++iter)
	{
		delete iter->second;
	}

	fontList.clear();
}

Json::Value* ResourceMgr::getJson(const std::string& dir)
{
	if (jsonList.find(dir) == jsonList.end())
	{
		// It doesn't exists! load it!
		std::vector<char>* data = load(dir);


		Json::Value* json = new Json::Value();

		if(data == NULL)
		{
			// The data is bad
			return NULL;
		}
		else
		{
			// The data is good
			Json::Reader reader;

			bool parsingSuccessful = reader.parse(std::string(data->begin(), data->end()), *json, false);
			if (!parsingSuccessful)
			{
				// report to the user the failure and their locations in the document.
				kf_log("Could not parse " + dir + ":- " + reader.getFormatedErrorMessages());

				delete json;
				delete data;
				return NULL;
			}

		}

		delete data; // delete the loaded data, it isn't needed anymore
		jsonList[dir] = json; // add it to the map

		return jsonList[dir];
	}
	else
	{
		// It exists!
		return jsonList[dir];
	}
}

bool ResourceMgr::destroyJson(const std::string& dir)
{
	std::map<std::string, Json::Value*>::iterator it = jsonList.find(dir);

	if (it != jsonList.end()) {
		delete it->second;
		jsonList.erase(it);

		return true;
	}

	return false;
}

void ResourceMgr::destroyAllJsons()
{
	std::map<std::string, Json::Value*>::iterator iter;
	for (iter = jsonList.begin(); iter != jsonList.end(); ++iter)
	{
		delete iter->second;
	}

	fontList.clear();
}

std::vector<char>* ResourceMgr::load(const std::string& dir)
{
	std::ifstream infile;
	infile.open(dir, std::ios::binary);
	if(!infile.good())
	{
		//the directory is bad!

		kf_log("Could not load '" + dir + "'");
		return NULL;
	}

	infile.seekg(0, std::ios::end);
	size_t size = infile.tellg();

	std::vector<char>* data = new std::vector<char>();
	data->resize(size);

	infile.seekg(0, std::ios::beg);
	infile.read(&data->front(), size);

	kf_log("Loaded '" + dir + "'");
	return data;
}

void ResourceMgr::destroyAll()
{
	destroyAllTextures();
	destroyAllFonts();
	destroyAllSounds();
	destroyAllJsons();
}