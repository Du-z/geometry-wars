#include "app\screens\mpLobby.h"

#include "kf\kf_log.h"

#include "SFML\Graphics.hpp"
#include "SFML\Window.hpp"
#include "kf\kf_vector2.h"

#include "app\eventMgr\event\uiElementClicked.h"

#include "app\resourceMgr.h"
#include "app\soundMgr.h"

MpLobby::MpLobby()
{
	m_isConnected = false;

	m_resendTimer = 10.0f; // make it connect straight away
	m_heartbeatSendTimer = 0;
	m_lastHeartbeatTimer = 0;
}

MpLobby::~MpLobby()
{

}

void MpLobby::setup(sf::RenderWindow& window)
{	
	//*** General Setup **//
	std::string screenName = "mpLobby";

	std::hash<std::string> hashFn;
	nameHash = hashFn(screenName);

	//*** ADD GAME OBJECTS **//

	Json::Value& screens = *ResourceMgr::GetInstance()->getJson("data/config/screens.json");
	Json::Value& screenConfig = screens[screenName];

	actorMgr.addActors(screenConfig["actors"], NULL);

	// sort the list based on enum order
	actorMgr.sortActors();

	//*** ADD LABELS **//
	userListStr = "Attempting to connect.";
	userListLbl.setup("userListLbl", userListStr, "data/arial.ttf",
		sf::Vector2f(sf::Vector2f(screenWidth / 2, 150)),
		30, sf::Color(255, 255, 255, 230));


	//*** SETUP NETWORKING **//
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		return;
		kf_log("WSAStartup failed.");
	}

	send_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(send_socket == SOCKET_ERROR)
	{
		return;
		kf_log("Error Opening socket: Error " + WSAGetLastError());
	}

	send_address.sin_family = AF_INET;
	send_address.sin_port = htons(2014);
	//send_address.sin_addr.s_addr = inet_addr("10.40.61.255");
	send_address.sin_addr.s_addr = inet_addr("127.0.0.1");

	send_address_broadcast = send_address;

	DWORD dwBytesReturned = 0;
	BOOL bNewBehavior = FALSE;
	WSAIoctl(send_socket, SIO_UDP_CONNRESET, &bNewBehavior, sizeof(bNewBehavior), NULL, 0, &dwBytesReturned, NULL, NULL);

	kf_log("Network setup complete.");
}

void MpLobby::simulation(sf::RenderWindow& window, float deltaT)
{
	sf::Event ev;
	while (window.pollEvent(ev))
	{
		if (ev.type == sf::Event::Closed)
		{
			nextScreen = EXIT;
		}
	}

	// simulate the vector of actors
	actorMgr.simulation(window, deltaT);

	netLoop(deltaT);
	
	userListLbl.setString(userListStr);
}

void MpLobby::netLoop(const float deltaT)
{
	int waiting;
	do
	{
		fd_set checksockets;
		checksockets.fd_count = 1;
		checksockets.fd_array[0]=send_socket;

		struct timeval t;
		t.tv_sec = 0;
		t.tv_usec = 0;

		waiting = select(NULL, &checksockets, NULL, NULL, &t);
		if(waiting > 0)
		{
			// read packet
			char buffer[1024]; // something big enough for any valid packet
			sockaddr_in from;
			int fromlength = sizeof(from);
			int result = recvfrom(send_socket, buffer, sizeof(buffer), 0, (SOCKADDR*)&from, &fromlength);  // read the data into the temp buffer
			if (result == SOCKET_ERROR)
			{
				//std::cout << "recvfrom failed with error " << WSAGetLastError() << std::endl;
			}
			else
			{
				ChatPacket* p = (ChatPacket*)buffer;  // cast the buffer's address as a packet pointer.
				switch(p->m_type)  // choose action based on the packet's type field
				{
				case HANDSHAKE_ACK:
					{
						recvHandshakeAck((ChatPacketHandshakeAck*)p, from);
						break;
					}
				case MESSAGE_FROM_SERVER:
					{
						recvMsg((ChatPacketMsgFromServer*)p);
						break;
					}
				case HEARTBEAT:
					{
						recvHeartbeat((ChatPacketHeartbeat*)p);
						break;
					}
				}
			}
		}
	}
	while(waiting);

	if(m_isConnected)
	{
		// if the is a connection

		// increment the heartbeat timers
		m_lastHeartbeatTimer += deltaT;
		m_heartbeatSendTimer += deltaT;

		if(m_lastHeartbeatTimer > 10)// if it hasnt gotten a heartbeat for more than 10 seconds
		{
			disconnect();

			kf_log(std::string("Lost connection to server ") + inet_ntoa(send_address.sin_addr) + ".");
		}
		else if(m_heartbeatSendTimer > 3.0f)// send a heartbeat packet every 3 seconds
		{
			sendHeartbeat();
			m_heartbeatSendTimer = 0;
		}

	}
	else
	{
		// if there is no connection
		m_resendTimer += deltaT;

		if(m_resendTimer > 3.0f) // try to connect every 3 seconds
		{
			sendHandshake();
			m_resendTimer = 0;
		}
	}
}

void MpLobby::disconnect()
{
	if(m_isConnected) // if we have not disconnected, tell the server that we are.
	{
		ChatPacketDisconnect packet(m_serverSlot);

		int result = sendto(send_socket, (const char*)&packet, sizeof(packet), 0, (SOCKADDR*)&send_address, sizeof(send_address));

		if(result == SOCKET_ERROR)
		{
			std::stringstream sstream;
			sstream << "Error: Disconnect could not be sent " << WSAGetLastError();
			kf_log(sstream.str());
			return;
		}
	}

	send_address = send_address_broadcast;

	m_isConnected = false;
}

void MpLobby::sendHandshake()
{
	kf_log(std::string("Atempting connection to server at ") + inet_ntoa(send_address.sin_addr) + ".");

	ChatPacketHandshake packet;

	int result = sendto(send_socket, (const char*)&packet, sizeof(packet), 0, (SOCKADDR*)&send_address, sizeof(send_address));

	if(result == SOCKET_ERROR)
	{
		std::stringstream sstream;
		sstream << "Error: Handshake could not be sent " << WSAGetLastError();
		kf_log(sstream.str());
		return;
	}
}

void MpLobby::recvHandshakeAck(const ChatPacketHandshakeAck* packet, const sockaddr_in& fromAdr)
{
	m_serverSlot = packet->m_slot;
	m_resendTimer = 0;
	m_isConnected = true;
	m_lastHeartbeatTimer = 0;

	send_address = fromAdr;

	kf_log(std::string("Connected to server at ") + inet_ntoa(send_address.sin_addr));
}

void MpLobby::recvMsg(const ChatPacketMsgFromServer* p)
{
	// display the message in the textbox
	userListStr = p->m_message;

	// tell the server that we got the message
	ChatPacketMsgAck packet(m_serverSlot, p->m_timestamp);

	int result = sendto(send_socket, (const char*)&packet, sizeof(packet), 0, (SOCKADDR*)&send_address, sizeof(send_address));

	if(result == SOCKET_ERROR)
	{
		std::stringstream sstream;
		sstream << "Error: Message conformation could not be sent " << WSAGetLastError();
		kf_log(sstream.str());
		return;
	}
}

void MpLobby::recvHeartbeat(const ChatPacketHeartbeat* packet)
{
	m_lastHeartbeatTimer = 0;

	if(VERBOSE)
	{
		kf_log("Received heartbeat from server.");
	}
}

void MpLobby::sendHeartbeat()
{
	ChatPacketHeartbeat packet(m_serverSlot);

	int result = sendto(send_socket, (const char*)&packet, sizeof(packet), 0, (SOCKADDR*)&send_address, sizeof(send_address));

	if(result == SOCKET_ERROR)
	{
		std::stringstream sstream;
		sstream << "Error: Heartbeat could not be sent " << WSAGetLastError();
		kf_log(sstream.str());
		return;
	}
}

void MpLobby::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::UI_ELEMENT_CLICKED)
	{
		UiElementClicked* uiElementClicked = static_cast<UiElementClicked*>(theEvent);

		if(uiElementClicked->getUiType() == Ui::BUTTON)
		{
			size_t nameHash = uiElementClicked->getElementName();
			std::hash<std::string> hashFn;

			if(nameHash == hashFn("backBtn"))
			{
				disconnect();
				
				nextScreen = MAIN_MENU;
				SoundMgr::GetInstance()->play("data/sounds/ui/click.ogg");
			}
			else
				nextScreen = NO_CHANGE;
		}
	}
}

void MpLobby::render(sf::RenderWindow& window)
{
	window.clear();

	// draw the vector of actors
	actorMgr.render(window);

	userListLbl.render(window);

	window.display();
}

void MpLobby::dispose()
{	
	actorMgr.dispose();
	userListLbl.dispose();
}