#include "app\screens\pause.h"

#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "kf/kf_vector2.h"

#include "app\eventMgr\event\keyPressed.h"

#include "app\resourceMgr.h"
#include "app\eventMgr.h"
#include "app\soundMgr.h"

#include "app\eventMgr\event\uiElementClicked.h"

Pause::Pause()
{
	paused = false;
}

Pause::~Pause()
{

}

void Pause::setup(sf::RenderWindow& window)
{
	//*** General Setup **//

	std::string screenName = "pause";

	std::hash<std::string> hashFn;
	nameHash = hashFn(screenName);

	//*** ADD GAME OBJECTS **//

	Json::Value& screens = *ResourceMgr::GetInstance()->getJson("data/config/screens.json");
	Json::Value& screenConfig = screens[screenName];

	actorMgr.addActors(screenConfig["actors"], NULL);

	// sort the list based on enum order
	actorMgr.sortActors();
}

void Pause::simulation(sf::RenderWindow& window, float deltaT)
{
	sf::Event ev;
	while (window.pollEvent(ev))
	{
		if (ev.type == sf::Event::Closed)
		{
			nextScreen = EXIT;
		}
	}

	// simulate the vector of actors
	actorMgr.simulation(window, deltaT);
}

void Pause::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::UI_ELEMENT_CLICKED)
	{
		UiElementClicked* uiElementClicked = static_cast<UiElementClicked*>(theEvent);

		if(uiElementClicked->getUiType() == Ui::BUTTON)
		{
			size_t nameHash = uiElementClicked->getElementName();
			std::hash<std::string> hashFn;

			if(nameHash == hashFn("continueBtn"))
			{
				paused = false;
				SoundMgr::GetInstance()->unpauseAll();
			}
			else if(nameHash == hashFn("restartBtn"))
			{
				nextScreen = GAME_LOOP_SP;
				SoundMgr::GetInstance()->stopAll();
			}
			else if(nameHash == hashFn("mainMenuBtn"))
			{
				nextScreen = MAIN_MENU;
				SoundMgr::GetInstance()->stopAll();
			}
			
			if(nextScreen != NO_CHANGE && nextScreen != EXIT || !paused)
			{
				SoundMgr::GetInstance()->play("data/sounds/ui/click.ogg");
			}
				
		}
	}
}

void Pause::render(sf::RenderWindow& window)
{
	window.clear();

	// draw the vector of actors
	actorMgr.render(window);

	window.display();
}

void Pause::dispose()
{
	actorMgr.dispose();
}