#ifndef H_HOW_PLAY
#define H_HOW_PLAY

#include "app\screen.h"

class HowPlay : public ABScreen
{
public:
	HowPlay();
	virtual ~HowPlay();

	virtual void setup(sf::RenderWindow& window);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
};

#endif // H_HOW_PLAY