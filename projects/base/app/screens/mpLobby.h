#ifndef H_MP_LOBBY
#define H_MP_LOBBY

#include "app\screen.h"
#include "app\actorMgr\actor\uiMgr\uiElement\label.h"

#include "winsock2.h"
#include "mswsock.h"

#include "chatPacket.h"

#define VERBOSE false

class MpLobby : public ABScreen
{
public:
	MpLobby();
	virtual ~MpLobby();

	virtual void setup(sf::RenderWindow& window);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void netLoop(const float deltaT);

	void sendHandshake(); // send a handshake to the server
	void recvHandshakeAck(const ChatPacketHandshakeAck* packet, const sockaddr_in& fromAdr);
	void recvMsg(const ChatPacketMsgFromServer* packet);
	void recvHeartbeat(const ChatPacketHeartbeat* packet); // updates m_lastHeartbeatTimer
	void sendHeartbeat();  // send a heartbeat to the server

	void disconnect(); // Disconnect from the server, if not already, set values to default

private:

	Label userListLbl;
	std::string userListStr;

	bool m_isConnected;

	sockaddr_in send_address;
	sockaddr_in send_address_broadcast;
	SOCKET send_socket;

	unsigned char m_serverSlot;
	float m_resendTimer;
	float m_heartbeatSendTimer;
	float m_lastHeartbeatTimer;
};

#endif // H_MP_LOBBY