#include "app\screens\mainMenu.h"

#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "kf/kf_vector2.h"

#include "app\eventMgr\event\keyPressed.h"
#include "app\eventMgr\event\uiElementClicked.h"

#include "app\resourceMgr.h"
#include "app\eventMgr.h"
#include "app\soundMgr.h"


MainMenu::MainMenu()
{

}

MainMenu::~MainMenu()
{

}

void MainMenu::setup(sf::RenderWindow& window)
{
	//*** General Setup **//

	std::string screenName = "mainMenu";

	std::hash<std::string> hashFn;
	nameHash = hashFn(screenName);
	
	//*** ADD GAME OBJECTS **//

	Json::Value& screens = *ResourceMgr::GetInstance()->getJson("data/config/screens.json");
	Json::Value& screenConfig = screens[screenName];

	actorMgr.addActors(screenConfig["actors"], NULL);

	// sort the list based on enum order
	actorMgr.sortActors();


}

void MainMenu::simulation(sf::RenderWindow& window, float deltaT)
{
	sf::Event ev;
	while (window.pollEvent(ev))
	{
		if (ev.type == sf::Event::Closed)
		{
			nextScreen = EXIT;
		}
	}

	// simulate the vector of actors
	actorMgr.simulation(window, deltaT);

	if(nextScreen != NO_CHANGE && nextScreen != EXIT)
		SoundMgr::GetInstance()->play("data/sounds/ui/click.ogg");
}

void MainMenu::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::UI_ELEMENT_CLICKED)
	{
		UiElementClicked* uiElementClicked = static_cast<UiElementClicked*>(theEvent);

		if(uiElementClicked->getUiType() == Ui::BUTTON)
		{
			size_t nameHash = uiElementClicked->getElementName();
			std::hash<std::string> hashFn;

			if(nameHash == hashFn("playBtnSp"))
			{
				nextScreen = GAME_LOOP_SP;
			}
			else if(nameHash == hashFn("playBtnMp"))
			{
				nextScreen = MP_LOBBY;
			}
			else if(nameHash == hashFn("howPlay"))
			{
				nextScreen = HOW_PLAY;
			}
			else if(nameHash == hashFn("exitBtn"))
			{
				nextScreen = EXIT;
			}
			else
				nextScreen = NO_CHANGE;
		}
	}
}

void MainMenu::render(sf::RenderWindow& window)
{
	window.clear();

	// draw the vector of actors
	actorMgr.render(window);

	window.display();
}

void MainMenu::dispose()
{
	actorMgr.dispose();
}