#ifndef H_MAIN_MENU
#define H_MAIN_MENU

#include "app\screen.h"

class MainMenu : public ABScreen
{
public:
	MainMenu();
	virtual ~MainMenu();

	virtual void setup(sf::RenderWindow& window);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
};

#endif // H_MAIN_MENU