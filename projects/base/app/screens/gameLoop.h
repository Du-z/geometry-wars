#ifndef H_GAME_LOOP
#define H_GAME_LOOP

#include "app\screen.h"
#include "app\eventMgr\collisionMgr.h"

#include "app\screens\pause.h"

class GameLoop : public ABScreen
{
public:
	GameLoop();
	virtual ~GameLoop();

	virtual void setup(sf::RenderWindow& window);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	static b2World* pB2World;

private:

	Pause* pauseScreen;

	CollisionMgr* collisionMgr;
};

#endif // H_GAME_LOOP