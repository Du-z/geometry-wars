#include "app\screens\howPlay.h"

#include "SFML\Graphics.hpp"
#include "SFML\Window.hpp"
#include "kf\kf_vector2.h"

#include "app\eventMgr\event\uiElementClicked.h"

#include "app\resourceMgr.h"
#include "app\soundMgr.h"

HowPlay::HowPlay()
{

}

HowPlay::~HowPlay()
{

}

void HowPlay::setup(sf::RenderWindow& window)
{	
	//*** General Setup **//
	std::string screenName = "howPlay";

	std::hash<std::string> hashFn;
	nameHash = hashFn(screenName);

	//*** ADD GAME OBJECTS **//

	Json::Value& screens = *ResourceMgr::GetInstance()->getJson("data/config/screens.json");
	Json::Value& screenConfig = screens[screenName];

	actorMgr.addActors(screenConfig["actors"], NULL);

	// sort the list based on enum order
	actorMgr.sortActors();
}

void HowPlay::simulation(sf::RenderWindow& window, float deltaT)
{
	sf::Event ev;
	while (window.pollEvent(ev))
	{
		if (ev.type == sf::Event::Closed)
		{
			nextScreen = EXIT;
		}
	}

	// simulate the vector of actors
	actorMgr.simulation(window, deltaT);
}

void HowPlay::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::UI_ELEMENT_CLICKED)
	{
		UiElementClicked* uiElementClicked = static_cast<UiElementClicked*>(theEvent);

		if(uiElementClicked->getUiType() == Ui::BUTTON)
		{
			size_t nameHash = uiElementClicked->getElementName();
			std::hash<std::string> hashFn;

			if(nameHash == hashFn("backBtn"))
			{
				nextScreen = MAIN_MENU;
				SoundMgr::GetInstance()->play("data/sounds/ui/click.ogg");
			}
			else
				nextScreen = NO_CHANGE;
		}
	}
}

void HowPlay::render(sf::RenderWindow& window)
{
	window.clear();

	// draw the vector of actors
	actorMgr.render(window);

	window.display();
}

void HowPlay::dispose()
{
	actorMgr.dispose();
}