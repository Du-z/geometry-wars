#ifndef H_GENERAL
#define H_GENERAL

#define _USE_MATH_DEFINES // to enable non-std math const

#include <stdlib.h>
#include <math.h>
#include <string>

#include "SFML/Graphics/Color.hpp"
#include "SFML/System/Vector2.hpp"
#include "Box2D/Common/b2Math.h"

static const std::string appName = "Geometry Wars";

static const int screenWidth = 1280;
static const int screenHeight = 720;

static const int mapHeight = 2048;
static const int mapWidth = 2048;

static const char worldScale = 16;

static const float screenWidthM = (float) screenWidth / worldScale;
static const float screenHeightM = (float) screenHeight / worldScale;

static float randFloat(float min, float max)
{
	float r = (float)rand() / (float)RAND_MAX;
	float retVal = min + r * (max - min);
	return retVal;
}

static int randInt(int min, int max)
{
	int retVal = min + (rand() % (int)(max - min + 1));
	return retVal;
}

static sf::Color randColour(const sf::Color& min, const sf::Color& max)
{
	sf::Color colour;

	colour.r = randInt(min.r, max.r);
	colour.g = randInt(min.g, max.g);
	colour.b = randInt(min.b, max.b);
	colour.a = randInt(min.a, max.a);

	return colour;
}

static float radToDeg(const float rad)
{
	return rad * 180 / (float)M_PI;
}

static float degToRad(const float deg)
{
	return deg / 180 * (float)M_PI;
}

static float vectToSpeed(const sf::Vector2f& vec)
{
	return sqrt(pow(vec.x / worldScale, 2) + pow(vec.y / worldScale, 2));
}

static float vectToSpeed(const b2Vec2& vec)
{
	return sqrt(pow(vec.x, 2) + pow(vec.y, 2));
}

static float vectToRadian(const sf::Vector2f& vec)
{
	return atan2(vec.y, vec.x);
}

static float vectToRadian(const sf::Vector2i& vec)
{
	return atan2((float)vec.y, (float)vec.x);
}

static float vectToRadian(const b2Vec2& vec)
{
	return atan2(vec.y, vec.x);
}

static float vectToDeg(const sf::Vector2f& vec)
{
	return radToDeg(atan2(vec.y, vec.x));
}

static float vectToDeg(const sf::Vector2i& vec)
{
	return radToDeg(atan2((float)vec.y, (float)vec.x));
}

static float vectToDeg(const b2Vec2& vec)
{
	return radToDeg(atan2(vec.y, vec.x));
}

static sf::Vector2f speedRotToSFVect(const float speed, const float radian)
{
	return sf::Vector2f(speed * cos(radian), speed * sin(radian));
}

static b2Vec2 speedRotToB2Vect(const float speed, const float radian)
{
	return b2Vec2(speed * cos(radian), speed * sin(radian));
}

// v1 = target
static float vectToRadian(const b2Vec2& v1, const b2Vec2& v2)
{
	float x = v1.x - v2.x;
	float y = v1.y - v2.y;

	return atan2(y, x);
}

// v1 = target
static float vectToDist(const b2Vec2& v1, const b2Vec2& v2)
{
	float adj = 0;
	float angle = vectToRadian(v1, v2);

	adj = v1.x - v2.x;
	return adj / cos(angle);

	return 0;
}

#endif // H_GENERAL